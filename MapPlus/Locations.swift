//
//  File.swift
//  MapPlus
//
//  Created by Toremurat Zholayev on 15.03.17.
//  Copyright © 2017 Zholayev Toremurat. All rights reserved.
//

import Foundation
import SwiftyJSON
struct Locations {
    var id:Int
    var map_id:Int
    var coords:String
    var titleRu:String
    var titleEn:String
    var titleKz:String
    var addressRu:String
    var addressEn:String
    var addressKaz:String
    
    init?(json:JSON?) {
        guard let json = json, let id = json["id"].int,
        let map_id = json["map_id"].int,
        let coords = json["coords"].string,
        let titleRu = json["label_ru"].string,
        let titleEn = json["label_en"].string,
        let titleKz = json["label_kz"].string,
        let addressRu = json["address_ru"].string,
        let addressEn = json["address_en"].string,
        let addressKaz = json["address_kz"].string
        
        else {
            print("locations json error")
            return nil
        }
        self.id = id
        self.map_id = map_id
        self.coords = coords
        self.titleEn = titleEn
        self.titleKz = titleKz
        self.titleRu = titleRu
        self.addressEn = addressEn
        self.addressRu = addressRu
        self.addressKaz = addressKaz
    }
}
