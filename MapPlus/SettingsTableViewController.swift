//
//  SettingsTableViewController.swift
//  MapPlus
//
//  Created by Toremurat Zholayev on 08.02.17.
//  Copyright © 2017 Zholayev Toremurat. All rights reserved.
//

import UIKit
import SVProgressHUD
class SettingsTableViewController: UITableViewController {
    
    @IBOutlet weak var actualLabel: UILabel!
    @IBOutlet weak var factLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var roleLabel: UILabel!
    @IBOutlet weak var iinLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    var userInfo = Profile()
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        userInfo.getInfo(completion: { [weak self] (info) in
            guard let vc = self else {return}
            vc.userInfo = info
            vc.nameLabel.text = info.name
            vc.iinLabel.text = info.iin
            vc.phoneLabel.text = info.phone
            vc.emailLabel.text = info.email
            vc.factLabel.text = info.legal_address
            vc.actualLabel.text = info.actual_address
        })
    }
    
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 4
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if section == 0 {
            return 3
        }
        if section == 1 {
            return 1
        }
        if section == 2 {
            return 4
        }
        else {
            return 4
        }
        
        
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 3 && indexPath.row == 3 {
            UserDefaults.standard.removeObject(forKey: "userId")
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginViewController
            self.present(vc, animated: true, completion: nil)
        }
        if indexPath.section == 3 && indexPath.row == 2 {
            let vc = FirstPageViewController()
            vc.title = "Редактирование"
            vc.buttonName = "Сохранить"
            vc.userInfo = userInfo
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
        if indexPath.section == 3 && indexPath.row == 1 {
            let alert = UIAlertController(title: "Выберите действие", message: nil, preferredStyle: .actionSheet)
            let pushOnButton = UIAlertAction(title: "Включить уведомления", style: .default, handler: { (act) in
                SVProgressHUD.showSuccess(withStatus: "Вы теперь будете получать уведомления!")
            })
            let pushOffButton = UIAlertAction(title: "Уведомления выключены", style: .default, handler: { (act) in
                
            })
            let cancelButton = UIAlertAction(title: "Закрыть", style: .cancel, handler: nil)
            alert.addAction(pushOnButton)
            alert.addAction(pushOffButton)
            alert.addAction(cancelButton)
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let header = view as! UITableViewHeaderFooterView
        header.textLabel!.textColor = UIColor.gray
        header.textLabel!.font = UIFont.systemFont(ofSize: 13.0)
        
        header.textLabel!.frame = header.frame
        header.textLabel!.textAlignment = NSTextAlignment.left
    }
    
    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
