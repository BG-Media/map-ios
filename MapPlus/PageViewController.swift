//
//  PageViewController.swift
//  MapPlus
//
//  Created by Toremurat Zholayev on 27.01.17.
//  Copyright © 2017 Zholayev Toremurat. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
class PageViewController: UIViewController {
    var pageMenu : CAPSPageMenu?
    var isOnline = false
    override func viewDidLoad() {
        super.viewDidLoad()
        if isOnline {
           
            
        }
        setupPageMenu()
        print(isOnline)
        self.title = "Ваши данные"
        pageMenu?.menuScrollView.isDirectionalLockEnabled = true
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(PageViewController.catchNotification),
                                               name: NSNotification.Name(rawValue: "NextNotification"),
                                               object: nil)
        
 
    }
    func catchNotification(notification:NSNotification) -> Void {
        guard let userInfo = notification.userInfo, let index  = userInfo["index"] as? Int, let message = userInfo["message"] as? String else {
            return
        }
        pageMenu?.moveToPage(index)
        self.title = message
        
    }
    func setupPageMenu(){
        // MARK: - UI Setup
        self.navigationController?.navigationBar.isOpaque = false
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.black]
        
        // MARK: - Scroll menu setup
        
         let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
        
        // Initialize view controllers to display and place in array
        var controllerArray : [UIViewController] = []
        if isOnline {
     
            let controller2 = storyBoard.instantiateViewController(withIdentifier: "SecondPage") as! SecondPageTableViewController
            controller2.title = "1"
            controller2.nextIndex = 1
            controllerArray.append(controller2)
            
            
            let controller3 = storyBoard.instantiateViewController(withIdentifier: "ThirdPage") as! ThirdPageViewController
            controller3.title = "2"
            controller3.isOnline = true
            controllerArray.append(controller3)
    
        }
        else {
            let controller1 : FirstPageViewController = FirstPageViewController()
            controller1.title = "1"
            controllerArray.append(controller1)
            let controller2 = storyBoard.instantiateViewController(withIdentifier: "SecondPage") as! SecondPageTableViewController
            controller2.title = "2"
            controllerArray.append(controller2)
           
            let controller3 = storyBoard.instantiateViewController(withIdentifier: "ThirdPage") as! ThirdPageViewController
            controller3.title = "3"
            controllerArray.append(controller3)
            let controller4 : FourPageViewController = FourPageViewController()
            controller4.title = "4"
            controllerArray.append(controller4)
        }
        
        // Customize menu (Optional)
        let parameters: [CAPSPageMenuOption] = [
            .scrollMenuBackgroundColor(UIColor.white),
            .viewBackgroundColor(UIColor.white),
            .selectionIndicatorColor(UIColor(red:0.98, green:0.37, blue:0.09, alpha:1.0)),
            .unselectedMenuItemLabelColor(UIColor.gray),
            .selectedMenuItemLabelColor(UIColor.black),
            .menuItemFont(UIFont.systemFont(ofSize: 15.0)),
            .menuHeight(44.0),
            .menuItemWidth(self.view.frame.width/5),
            .addBottomMenuHairline(true),
            .bottomMenuHairlineColor(UIColor(red:0.92, green:0.92, blue:0.95, alpha:1.0)),
            .selectionIndicatorHeight(2.0),
            .centerMenuItems(true)
        ]
        
        // Initialize scroll menu
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0.0, y: 0.0, width: self.view.frame.width, height: self.view.frame.height), pageMenuOptions: parameters)
        
        self.addChildViewController(pageMenu!)
        self.view.addSubview(pageMenu!.view)
        
        pageMenu!.didMove(toParentViewController: self)
    }
    
    func didTapGoToLeft() {
        let currentIndex = pageMenu!.currentPageIndex
        
        if currentIndex > 0 {
            pageMenu!.moveToPage(currentIndex - 1)
        }
    }
    
    func didTapGoToRight() {
        let currentIndex = pageMenu!.currentPageIndex
        
        if currentIndex < pageMenu!.controllerArray.count {
            pageMenu!.moveToPage(currentIndex + 1)
        }
    }
    
    // MARK: - Container View Controller
    override var shouldAutomaticallyForwardAppearanceMethods : Bool {
        return true
    }
    
    override func shouldAutomaticallyForwardRotationMethods() -> Bool {
        return true
    }
    
}
