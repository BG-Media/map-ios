//
//  User.swift
//  MapPlus
//
//  Created by Toremurat Zholayev on 06.02.17.
//  Copyright © 2017 Zholayev Toremurat. All rights reserved.
//

import Foundation
import SVProgressHUD
import Alamofire


let modelName = UIDevice.current.model

struct User {
    private let email:String
    private let password:String
    private let token:String
    
    init(email:String, password:String, token:String) {
        self.email = email
        self.password = password
        self.token = token
    }
    
    func registerUser(completion: @escaping () -> Void)
    {
        SVProgressHUD.show()
        Alamofire.request(BaseUrl.mainUrl + BaseUrl.signUp, method: .post,
                parameters: ["email":email,
                "password":password,
                "device_token":token,
                "device_name":modelName]).responseJSON { response in
                    switch response.result {
                        case .success:
                            if let json = response.result.value as? [String:AnyObject] {
                                print(json)
                                if let status = json["status"] as? String, let data = json["result"] as? [String:AnyObject] {
                                    if status == "success"{
                                        print(data)
                                        UserDefaults.standard.set(self.email, forKey: "email")
                                        UserDefaults.standard.set(self.password, forKey: "password")
                                        completion()
                                        SVProgressHUD.showSuccess(withStatus: "Регистрация успешно выполнена!")
                                    }
                                    else if status == "error" {
                                        if let error = data["errors"] as? [String] {
                                            SVProgressHUD.showError(withStatus: error[0])
                                        }
                                      
                                    }
                                }
                               
                            }
                        case .failure(let error):
                            print(error)
                            SVProgressHUD.showError(withStatus: error.localizedDescription)
                        
                    }
                }
    }
    func loginUser(completion: @escaping () -> Void)
    {
        SVProgressHUD.show()
        print(token)
        var tokenIos = token
        if email == "admin@admin.com" {
            tokenIos = "sdf03go"
        }
        Alamofire.request(BaseUrl.mainUrl + BaseUrl.signIn, method: .post,
                parameters: ["email":email,
               "password":password,
               "device_token":tokenIos,
               "device_name":modelName]).responseJSON { response in
                switch response.result {
                    case .success:
                        if let json = response.result.value as? [String:AnyObject] {
                            if let status = json["status"] as? String {
                                if let data = json["result"] as? [String:AnyObject] {
                                    if status == "success"{
                                        print(data)
                                        UserDefaults.standard.set(1, forKey: "userId")
                                        completion()
                                        SVProgressHUD.dismiss()
                                    }
                                }
                                else if status == "error" {
                                    if let error = json["result"] as? String {
                                        SVProgressHUD.showError(withStatus: error)
                                    }
                                    
                                }
                            }
                            
                        }
                    case .failure(let error):
                        print(error)
                        SVProgressHUD.showError(withStatus: error.localizedDescription)
                        
                }
        }
    }
    func getProfileInfo(completion: @escaping () -> Void){
        
    }
  
  
    
}
