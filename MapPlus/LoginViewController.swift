//
//  ViewController.swift
//  MapPlus
//
//  Created by Toremurat Zholayev on 10.01.17.
//  Copyright © 2017 Zholayev Toremurat. All rights reserved.
//

import UIKit
import SVProgressHUD
class LoginViewController: UIViewController {

    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.hideKeyboardWhenTappedAround()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector:  #selector(keyboardWillHide), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
       
    }
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    func keyboardWillShow(sender: Notification) {
        self.view.frame.origin.y = -50
    }
    
    func keyboardWillHide(sender: Notification) {
        self.view.frame.origin.y = 0
    }
    @IBAction func loginButtonPressed(_ sender: UIButton) {
        guard let password = passwordTextField.text, !password.isEmpty, let email = emailTextField.text, !email.isEmpty  else {
            print("please fill everethx")
            return
        }
        let user = User(email: email, password: password, token: BaseUrl.token)
        user.loginUser { () in
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "TabBarVC") as! UITabBarController
            self.present(vc, animated: true, completion: nil)
        }
    }
    @IBAction func registerPressed(_ sender: Any) {
        self.performSegue(withIdentifier: "SegueRegister", sender: nil)
    }


}
extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    func moveToNextPageWith(index:Int, title:String) {
        
        let nc = NotificationCenter.default
        nc.post(name: Notification.Name(rawValue: "NextNotification"),
                object: nil,
                userInfo: ["index":index, "message":title])
    }
    func showLocalizedErrorWith(message:String) {
        SVProgressHUD.showError(withStatus: message)
        
    }
   
}

