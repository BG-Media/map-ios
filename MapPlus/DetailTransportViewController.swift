//
//  DetailTransportViewController.swift
//  MapPlus
//
//  Created by Toremurat Zholayev on 17.02.17.
//  Copyright © 2017 Zholayev Toremurat. All rights reserved.
//

import UIKit
import SDWebImage
class DetailTransportViewController: UIViewController, UIScrollViewDelegate {
    
    var carDetail:MyCars!
    
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var distanceHistory: UILabel!
    @IBOutlet weak var insuranceDateLabel: UILabel!
    @IBOutlet weak var inspirationDateLabel: UILabel!
    @IBOutlet weak var registerPlaceLabel: UILabel!
    @IBOutlet weak var govNumberLabel: UILabel!
    @IBOutlet weak var freeLabel: UILabel!
    @IBOutlet weak var usingLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    override func viewDidLoad() {
        
        super.viewDidLoad()
        scrollView.delegate = self

        collectionView.delegate = self
        collectionView.dataSource = self
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 1
        collectionView!.collectionViewLayout = layout
        collectionView.register(UINib.init(nibName: "GoodsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "Good")

       
        
        //Updating UI
        nameLabel.text = "\(carDetail.car.brand_name)  \(carDetail.car.model_name)"
        usingLabel.text = "Загружено: \(carDetail.using.volume) м³ - \(carDetail.using.capacity) т"
        freeLabel.text = "Свободно: \(carDetail.free.volume) м³ - \(carDetail.free.capacity) т"
        govNumberLabel.text = carDetail.gov_number
        registerPlaceLabel.text = carDetail.reg_place
        insuranceDateLabel.text = carDetail.insurance_policy
        inspirationDateLabel.text = carDetail.date_tech_inspection
        distanceHistory.text = "~ \(carDetail.distance_history) км \n~ \(carDetail.time_history) ч"
        descLabel.text = carDetail.desc
        statusLabel.text = self.getStatusName(id: carDetail.status)
        if carDetail.media.count != 0 {
            imageView.sd_setImage(with: URL(string: BaseUrl.mainUrl + carDetail.media[0].path), placeholderImage: UIImage(named: "mersedes"))
        }
        pageControl.numberOfPages = carDetail.media.count
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        print(scrollView.contentOffset)
        if scrollView.contentOffset.y < 0.0 {
            scrollView.contentOffset.y = 0.0
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        scrollView.contentOffset.y = 0.0
    }
    override func viewWillDisappear(_ animated: Bool) {
        if let navCtrl = self.navigationController {
            unchangeNavBar(Navigation: navCtrl)
        }
        super.viewWillDisappear(animated)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let navCtrl = self.navigationController {
            changeNavBar(Navigation: navCtrl)
        }
    }
    
    func changeNavBar(Navigation:UINavigationController){
        Navigation.navigationBar.tintColor = UIColor(red:0.98, green:0.37, blue:0.09, alpha:1.0)
        
        Navigation.navigationBar.isTranslucent = true
        Navigation.view.backgroundColor = UIColor.white
        Navigation.navigationBar.backgroundColor = UIColor.clear
    }
    func unchangeNavBar(Navigation:UINavigationController){
        
        Navigation.navigationBar.isTranslucent = false
        Navigation.view.backgroundColor = UIColor.white
        //Navigation.navigationBar.backgroundColor = UIColor.white
    }
    // MARK: ---------------------------TABLE VIEW DELEGATES
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
   
}
extension DetailTransportViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Good", for: indexPath as IndexPath) as! GoodsCollectionViewCell
        
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 90)
    }
}
