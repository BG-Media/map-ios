//
//  Type.swift
//  MapPlus
//
//  Created by Toremurat Zholayev on 15.03.17.
//  Copyright © 2017 Zholayev Toremurat. All rights reserved.
//

import Foundation
import SwiftyJSON
struct Type {
    var id:Int
    var name:String
    
    init?(json:JSON?) {
        guard let json = json, let id = json["id"].int,
        let name = json["name_ru"].string
        else {
                print("type json error")
                return nil
        }
        self.id = id
        self.name = name
        
    }
}
