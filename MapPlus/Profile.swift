//
//  Profile.swift
//  MapPlus
//
//  Created by Toremurat Zholayev on 10.02.17.
//  Copyright © 2017 Zholayev Toremurat. All rights reserved.
//

import Foundation
import SVProgressHUD
import Alamofire
import SwiftyJSON


struct Profile {
    private let device_token:String
    let name:String
    let iin:String
    let phone:String
    let email:String
    let legal_address:String
    let actual_address:String
    let sphere_id:Int
    let avatar:String
    let balance:Int
    
    init(){
        self.device_token = ""
        self.name = ""
        self.iin = ""
        self.phone = ""
        self.email = ""
        self.legal_address = ""
        self.actual_address = ""
        self.sphere_id = 0
        self.avatar = ""
        self.balance = 0
    }
    
    init(device_token:String, name:String, iin:String, phone:String, email:String, legal_address:String, actual_address:String, sphere_id:Int) {
        self.device_token = device_token
        self.name = name
        self.iin = iin
        self.phone = phone
        self.email = email
        self.legal_address = legal_address
        self.actual_address = actual_address
        self.sphere_id = sphere_id
        self.avatar = ""
        self.balance = 0
        
    }
    init? (json:JSON?) {
        guard let json = json,
        let name = json["name"].string,
        let phone = json["phone"].string,
        let email = json["email"].string,
        let avatar = json["avatar"].string,
        let iin = json["iin"].int,
        let actual_address = json["actual_address"].string,
        let legal_address = json["legal_address"].string,
        let sphere_id = json["sphere_id"].int,
        let balance = json["balance"].int else {
            print("parsing error")

            return nil
                    }
        self.device_token = BaseUrl.token
        self.name = name
        self.phone = phone
        self.iin = "\(iin)"
        self.email = email
        self.avatar = avatar
        self.actual_address = actual_address
        self.legal_address = legal_address
        self.sphere_id = sphere_id
        self.balance = balance
    }

    func updateProfile(completion: @escaping () -> Void)
    {
        SVProgressHUD.show()
        Alamofire.request(BaseUrl.mainUrl + BaseUrl.updateProfile, method: .post,
                          parameters: ["device_token":device_token,
                                       "name":name,
                                       "iin":iin,
                                       "phone":phone,
                                       "email":email,
                                       "legal_address":legal_address,
                                       "actual_address":actual_address,
                                       "sphere_id":sphere_id]).responseJSON { response in
                                        switch response.result {
                                        case .success:
                                            if let json = response.result.value as? [String:AnyObject] {
                                                if let status = json["status"] as? String, let data = json["result"] as? [String:AnyObject] {
                                                    if status == "success"{
                                                        print(data)
                                                        SVProgressHUD.dismiss()
                                                        completion()
                                                        
                                                    }
                                                    else if status == "error" {
                                                        if let error = data["errors"] as? [String] {
                                                            SVProgressHUD.showError(withStatus: error[0])
                                                        }
                                                        
                                                    }
                                                }
                                                
                                            }
                                        case .failure(let error):
                                            print(error)
                                            SVProgressHUD.showError(withStatus: error.localizedDescription)
                                            
                                        }
        }
    }
 
    func getInfo( completion: @escaping (_ info:Profile) -> Void){
        SVProgressHUD.show()
        Alamofire.request(BaseUrl.mainUrl + BaseUrl.myProfile, method: .post,
              parameters: ["device_token": BaseUrl.token]).responseJSON { response in
                    switch response.result {
                    case .success:
                        guard let data = response.data else {return }
                        let json = JSON(data:data)
                        print(json)
                        if let user:Profile = Profile(json: json["result"]) {
                            SVProgressHUD.dismiss()
                            completion(user)
                        }
                        
                        
                        
                    case .failure(let error):
                        print(error)
                        SVProgressHUD.showError(withStatus: error.localizedDescription)
                        
                    }
        }
    }
    
    
}
