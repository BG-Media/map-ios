//
//  TransListCollectionViewController.swift
//  MapPlus
//
//  Created by Toremurat Zholayev on 19.04.17.
//  Copyright © 2017 Zholayev Toremurat. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD
import SwiftyJSON
import SnapKit

class TransListCollectionViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    var myCollectionView: UICollectionView!
    var orderId = 0
    
    private var myCars: [MyCars] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor(red:0.99, green:0.38, blue:0.14, alpha:1.00)
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 1
       
        
        myCollectionView = UICollectionView(frame: self.view.bounds, collectionViewLayout: layout)
        myCollectionView.dataSource = self
        myCollectionView.delegate = self
        myCollectionView.register(UINib.init(nibName: "GoodsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier:
            "Good")
        myCollectionView.backgroundColor =  UIColor(red:0.95, green:0.95, blue:0.95, alpha:1.00)
        myCollectionView.isOpaque = false

        self.view.addSubview(myCollectionView)
        
        
        let label = UILabel()
        label.text = "Выберите транспорт"
        label.textAlignment = .center
        label.backgroundColor = UIColor(red:0.99, green:0.38, blue:0.14, alpha:1.00)
        label.textColor = UIColor.white
        label.font = UIFont.systemFont(ofSize: 20.0)
        self.view.addSubview(label)
        
        
        let closeButton = UIButton()
        closeButton.setImage(UIImage(named:"Delete_25"), for: .normal)
        closeButton.addTarget(self, action: #selector(TransListCollectionViewController.closeButtonPressed), for: .touchUpInside)
        self.view.addSubview(closeButton)
        
       
        label.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(40)
            make.left.equalToSuperview().offset(20)
            
            make.right.equalToSuperview().offset(-20)
            
        }
        closeButton.snp.makeConstraints { (make) in
            make.right.equalToSuperview().offset(-20)
            make.top.equalToSuperview().offset(40)
            make.width.equalTo(25)
            make.height.equalTo(25)
            
            
        }
        
        myCollectionView.snp.makeConstraints { (make) in
            make.top.equalTo(label.snp.bottom).offset(20)
            make.left.equalToSuperview().offset(0)
            make.right.equalToSuperview().offset(0)
            make.bottom.equalToSuperview().offset(-30)
        }
        getMyTransport()

       
    }
    
    func closeButtonPressed(){
        self.dismiss(animated: true, completion: nil)
    }

    

    // MARK: UICollectionViewDataSource

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return myCars.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
       let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Good", for: indexPath as IndexPath) as! GoodsCollectionViewCell
        cell.carNameLabel.text = "\(myCars[indexPath.row].car.brand_name) \(myCars[indexPath.row].car.model_name)"
        cell.carDescLabel.text = "\(myCars[indexPath.row].year) · \(myCars[indexPath.row].car.type) · \(myCars[indexPath.row].volume) м³ · \(myCars[indexPath.row].capacity) т "
        cell.carVolumeLabel.text = "Загружено: \(myCars[indexPath.row].using.volume) м³ - \(myCars[indexPath.row].using.capacity) т    Свободно: \(myCars[indexPath.row].free.volume) м³ - \(myCars[indexPath.row].free.capacity) т"
        cell.priceLabel.text = ""
        
        cell.carStatusLabel.text = getStatusName(id: myCars[indexPath.row].status)
        cell.carVolumeLabel.textColor = UIColor.black
    
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
     
        let cell = collectionView.cellForItem(at: indexPath)
        cell?.contentView.backgroundColor = UIColor(red:0.95, green:0.95, blue:0.95, alpha:1.00)
        
        
        let alert = UIAlertController(title: "Выберите действие", message: nil, preferredStyle: .actionSheet)
        let action = UIAlertAction(title: "Принять заказ", style: .default) {  (act) in
            self.takeOrder(orderId: self.orderId, carId: self.myCars[indexPath.row].id)
        }
        let cancel = UIAlertAction(title: "Закрыть", style: .cancel) { (act) in
            
        }
        alert.addAction(action)
        alert.addAction(cancel)
        present(alert, animated: true, completion: nil)
        

    }
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath)
        cell?.contentView.backgroundColor = UIColor.white
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 95)
    }
    func takeOrder(orderId:Int, carId:Int) {
        SVProgressHUD.show()
        Alamofire.request(BaseUrl.mainUrl + "/api/v1/transporter/order/takeOrder", method: .post,
                          parameters: ["device_token": BaseUrl.token,
                                       "order_id":orderId,
                                       "car_id":carId]).responseJSON {response in
                                        switch response.result {
                                        case .success:
                                            guard let data = response.data else {return }
                                            let json = JSON(data:data)
                                            
                                            if json["status"] == "success" {
                                                
                                                SVProgressHUD.showSuccess(withStatus: json["result"].string)
                                                self.dismiss(animated: true, completion: nil)
                                            }
                                            else if json["status"] == "error" {
                                                SVProgressHUD.showError(withStatus: json["result"]["error"].string)
                                            }
                                            
                                        case .failure(let error):
                                            print(error)
                                            SVProgressHUD.showError(withStatus: error.localizedDescription)
                                            
                                        }
        }
    }
    func getMyTransport() {
        SVProgressHUD.show()
        Alamofire.request(BaseUrl.mainUrl + BaseUrl.myCars, method: .post,
                          parameters: ["device_token": BaseUrl.token]).responseJSON { [weak self] response in
                            switch response.result {
                            case .success:
                                guard let vc = self, let data = response.data else {return }
                                let json = JSON(data:data)
                                print(json)
                                if json["status"] == "success" {
                                    for (_, subJson):(String, JSON) in json["result"] {
                                        if let newCar = MyCars(json: subJson) {
                                            vc.myCars.append(newCar)
                                        }
                                    }
                                    SVProgressHUD.dismiss()
                                    vc.myCollectionView!.reloadData()
                                }
                            case .failure(let error):
                                print(error)
                                SVProgressHUD.showError(withStatus: error.localizedDescription)
                                
                            }
        }
        
    }

}
