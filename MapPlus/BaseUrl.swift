//
//  Utils.swift
//  MapPlus
//
//  Created by Toremurat Zholayev on 06.02.17.
//  Copyright © 2017 Zholayev Toremurat. All rights reserved.
//

import Foundation
import UIKit
struct BaseUrl {
    
    static let mainUrl = "http://mapplus.bugingroup.kz"
    static let signUp = "/api/v1/transporter/account/signUp"
    static let signIn = "/api/v1/transporter/account/signIn"
    static let myProfile = "/api/v1/transporter/account/fetch"
    static let updateProfile = "/api/v1/transporter/account/update"
    static let addCar = "/api/v1/transporter/cars/addCar"
    static let listOptions = "/api/v1/transporter/cars/listOptions"
    static let addImage = "/api/v1/transporter/cars/addImage"
    static let myCars = "/api/v1/transporter/cars/myCars"
    static let filterParams = "/api/v1/transporter/filter/preLoad"
    static let getFilterList = "/api/v1/transporter/filter/getList"
    static let myOrders = "/api/v1/transporter/order/myOrders"
    static let token = UIDevice.current.identifierForVendor!.uuidString
    //UIDevice.current.identifierForVendor!.uuidString //"sdf03go"
    static let notifications = "/api/v1/transporter/account/notifications"
    static let resetPassword = "/api/v1/transporter/account/resetPassword"
    static let sendLocation = "/api/v1/transporter/locate/send"
}
