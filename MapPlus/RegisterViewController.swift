//
//  RegisterViewController.swift
//  MapPlus
//
//  Created by Toremurat Zholayev on 10.01.17.
//  Copyright © 2017 Zholayev Toremurat. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController {

    @IBOutlet weak var confirmPasswordTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.hideKeyboardWhenTappedAround()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector:  #selector(keyboardWillHide), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
        
    }
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    func keyboardWillShow(sender: Notification) {
        
        self.view.frame.origin.y = -50
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    func keyboardWillHide(sender: Notification) {
        self.view.frame.origin.y = 0
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    @IBAction func registerButtonPressed(_ sender: UIButton) {
       
        guard let password = passwordTextField.text, !password.isEmpty, let email = emailTextField.text, !email.isEmpty, let confirm = confirmPasswordTextField.text, !confirm.isEmpty  else {
            print("please fill everything")
            return
        }//
        
        if password == confirm {
            let user = User(email: email, password: password, token: BaseUrl.token)
            user.registerUser { act in
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "PageNC") as! UINavigationController
                self.present(vc, animated: true, completion: nil)
            }
        }
        
    }
    @IBAction func loginButtonPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func closeButtonPressed(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
}
