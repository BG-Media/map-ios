//
//  ForgotPasswordViewController.swift
//  MapPlus
//
//  Created by Toremurat Zholayev on 14.04.17.
//  Copyright © 2017 Zholayev Toremurat. All rights reserved.
//

import UIKit
import SVProgressHUD
import Alamofire
class ForgotPasswordViewController: UIViewController {

    @IBOutlet weak var emailTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
    }
    
    @IBAction func sendButtonPressed(_ sender: UIButton) {
        guard let email = emailTextField.text, !email.isEmpty else {
            return
        }
        resetPassword(email: email)
        
    }
    @IBAction func closeButtonPressed(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    func resetPassword(email:String)
    {
        SVProgressHUD.show()
        Alamofire.request(BaseUrl.mainUrl + BaseUrl.resetPassword, method: .post, parameters: ["email":email]).responseJSON { response in
            switch response.result {
            case .success:
                if let json = response.result.value as? [String:AnyObject] {
                    print(json)
                    if let status = json["status"] as? String {
                        if status == "success"{
                            if let res = json["result"] as? String {
                                SVProgressHUD.showSuccess(withStatus: res)
                            }
                            
                        }
                        else if status == "error" {
                            
                          
                                SVProgressHUD.showError(withStatus: "Неправильный email")
                            
                            
                        }
                    }
                    
                }
            case .failure(let error):
                print(error)
                SVProgressHUD.showError(withStatus: error.localizedDescription)
                
            }
        }
    }

}
