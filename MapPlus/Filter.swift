//
//  Filter.swift
//  MapPlus
//
//  Created by Toremurat Zholayev on 15.03.17.
//  Copyright © 2017 Zholayev Toremurat. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire
import SVProgressHUD
struct Filter {
    var locations:[Locations]
    var statuses:[String]
    var insured:[Type]
    var typeTransp:[Type]
    var carTypes:[Type]
    var hazardClass:[Type]
    
    init?(json:JSON?) {
        guard let json = json, let stats = json["statuses"].arrayObject else {
            print("json filter error 1")
            return nil
        }
        
        var loc:[Locations] = []
        var insured:[Type] = []
        var typeTransp:[Type] = []
        var carTypes:[Type] = []
        var hazardClass:[Type] = []
        
        for (_, subJson):(String, JSON) in json["locations"] {
            if let new = Locations(json: subJson) {
                loc.append(new)
            }
        }
        
        for (_, subJson):(String, JSON) in json["insured"] {
            if let new = Type(json: subJson) {
                insured.append(new)
            }
        }
        for (_, subJson):(String, JSON) in json["typeTransp"] {
            if let new = Type(json: subJson) {
                typeTransp.append(new)
            }
        }
        for (_, subJson):(String, JSON) in json["carTypes"] {
            if let new = Type(json: subJson) {
                carTypes.append(new)
            }
        }
        for (_, subJson):(String, JSON) in json["hazardClass"] {
            if let new = Type(json: subJson) {
                hazardClass.append(new)
            }
        }
        if let statuses = stats as? [String] {
            self.statuses = statuses
            self.locations = loc
            self.insured = insured
            self.typeTransp = typeTransp
            self.carTypes = carTypes
            self.hazardClass = hazardClass
        }
        else {
            return nil
        }
        
        
        
    }
    
    
   
    func getInfo( completion: @escaping (_ info:Profile) -> Void){
        SVProgressHUD.show()
        Alamofire.request(BaseUrl.mainUrl + BaseUrl.myProfile, method: .post,
                          parameters: ["device_token": BaseUrl.token]).responseJSON { response in
                            switch response.result {
                            case .success:
                                guard let data = response.data else {return }
                                let json = JSON(data:data)
                                print(json)
                                if let user:Profile = Profile(json: json["result"]) {
                                    SVProgressHUD.dismiss()
                                    completion(user)
                                }
                                
                                
                                
                            case .failure(let error):
                                print(error)
                                SVProgressHUD.showError(withStatus: error.localizedDescription)
                                
                            }
        }
    }
    
    
}
