//
//  DetailGoodsViewController.swift
//  MapPlus
//
//  Created by Toremurat Zholayev on 10.01.17.
//  Copyright © 2017 Zholayev Toremurat. All rights reserved.
//

import UIKit
import SVProgressHUD
import Alamofire
import SwiftyJSON
class DetailGoodsViewController: UIViewController {
    var cancelButton:UIButton!
    @IBOutlet weak var acceptButton: UIButton!
    @IBOutlet weak var customerDescLabel: UILabel!
    @IBOutlet weak var customerNameLabel: UILabel!
    @IBOutlet weak var hazardLabel: UILabel!
    @IBOutlet weak var kindTransLabel: UILabel!
    @IBOutlet weak var typeTransLabel: UILabel!
    @IBOutlet weak var descriptionCharacLabel: UILabel!
    @IBOutlet weak var insuredLabel: UILabel!
    @IBOutlet weak var capacityLabel: UILabel!
    @IBOutlet weak var volumeLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var dateShipped: UILabel!
    @IBOutlet weak var descriptionDirectionLabel: UILabel!
    @IBOutlet weak var directionLabel: UILabel!
    
    @IBOutlet weak var statusLabel: UILabel!
    var detail:Ships!
    var isMyShips = false
    override func viewDidLoad() {
        super.viewDidLoad()
        if isMyShips {
            acceptButton.setTitle("Заказ доставлен", for: .normal)
            setupCancelButton(isHidden: false)
        }
        
        //self.navigationController?.navigationBar.isTranslucent = false
        directionLabel.text = "\(detail.from.titleRu) → \(detail.to.titleRu)"
        distanceLabel.text = "\(detail.distance) км"
        descriptionDirectionLabel.text = detail.desc
        dateShipped.text = detail.provisionalDate
        priceLabel.text = "\(detail.price) тг"
        volumeLabel.text = "\(detail.volume) м³"
        capacityLabel.text = "\(detail.capacity) т"
        insuredLabel.text = detail.insured
        descriptionCharacLabel.text = detail.desc
        typeTransLabel.text = detail.typeTransp
        kindTransLabel.text = detail.preferredCar
        hazardLabel.text = detail.hazardClass
        if detail.status == 0 {
            customerNameLabel.text = "Не доступно"
            customerDescLabel.text = "Не доступно"
        }
        else {
             customerNameLabel.text = detail.companyName
        }
        
        if detail.status >= 2 && detail.status <= 4 {
            acceptButton.isHidden = true
            cancelButton.isHidden = true
        }
        switch detail.status {
        
        case 1:
            statusLabel.text = "Статус заказа: Принят"
            break
        case 2:
            statusLabel.text = "Статус заказа: Отказано"
            break
        case 3:
            statusLabel.text = "Статус заказа: Доставлен"
            break
        case 4:
            statusLabel.text = "Статус заказа: Завершен"
        default:
            statusLabel.text = ""
        }
        
        
        // Do any additional setup after loading the view.
    }
    @IBAction func acceptOrderButtonPressed(_ sender: UIButton) {
        if isMyShips {
            deliveredOrder(orderId: detail.id)
        }
        else {
            
            
            let modalViewController = TransListCollectionViewController()
            modalViewController.modalPresentationStyle = .overCurrentContext
            modalViewController.orderId = detail.id
            present(modalViewController, animated: true, completion: nil)
            //takeOrder(orderId: detail.id, carId: 1)
        }
    }
    func setupCancelButton(isHidden:Bool){
        cancelButton = UIButton()
        print(detail.status)
        
        cancelButton.setTitle("Отказаться от груза", for: .normal)
        cancelButton.backgroundColor = UIColor(red:0.98, green:0.37, blue:0.09, alpha:1.00)
        cancelButton.tintColor = UIColor.white
        cancelButton.titleLabel?.font = UIFont.systemFont(ofSize: 16.0)
  
        cancelButton.addTarget(self, action: #selector(DetailGoodsViewController.cancelShipButtonPressed), for: .touchUpInside)
        view.addSubview(cancelButton)
        cancelButton.isHidden = isHidden
        cancelButton.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(0)
            make.right.equalToSuperview().offset(0)
            make.bottom.equalTo(acceptButton.snp.top).offset(-8)
            make.height.equalTo(40)
  
        }
    }
    func cancelShipButtonPressed(){
        
    }
    func deliveredOrder(orderId:Int) {
        SVProgressHUD.show()
        Alamofire.request(BaseUrl.mainUrl + "/api/v1/transporter/order/delivered", method: .post,
                          parameters: ["device_token": BaseUrl.token,
                                       "order_id":orderId]).responseJSON {response in
                                        switch response.result {
                                        case .success:
                                            guard let data = response.data else {return }
                                            let json = JSON(data:data)
                                            
                                            if json["status"] == "success" {
                                                
                                                SVProgressHUD.showSuccess(withStatus: json["result"].string)
                                                //self.navigationController?.popViewController(animated: true)
                                            }
                                            else if json["status"] == "error" {
                                                SVProgressHUD.showError(withStatus: json["result"]["error"].string)
                                            }
                                            
                                        case .failure(let error):
                                            print(error)
                                            SVProgressHUD.showError(withStatus: error.localizedDescription)
                                            
                                        }
        }
    }
    
    


    func takeOrder(orderId:Int, carId:Int) {
        SVProgressHUD.show()
        Alamofire.request(BaseUrl.mainUrl + "/api/v1/transporter/order/takeOrder", method: .post,
                          parameters: ["device_token": BaseUrl.token,
                                       "order_id":orderId,
                                       "car_id":carId]).responseJSON {response in
                            switch response.result {
                            case .success:
                                guard let data = response.data else {return }
                                let json = JSON(data:data)
         
                                if json["status"] == "success" {
                             
                                    SVProgressHUD.showSuccess(withStatus: json["result"].string)
                                }
                                else if json["status"] == "error" {
                                    SVProgressHUD.showError(withStatus: json["result"]["error"].string)
                                }
                                
                            case .failure(let error):
                                print(error)
                                SVProgressHUD.showError(withStatus: error.localizedDescription)
                                
                            }
        }
    }

   
}
