//
//  Model.swift
//  MapPlus
//
//  Created by Toremurat Zholayev on 14.02.17.
//  Copyright © 2017 Zholayev Toremurat. All rights reserved.
//

import Foundation
import SwiftyJSON

struct Model {
    var id:Int
    var name:String
    init? (json:JSON?) {
        guard let json = json, let id = json["id"].int, let name = json["name_ru"].string else {
            return nil
        }
        self.id = id
        self.name = name
    }
    
    
}
