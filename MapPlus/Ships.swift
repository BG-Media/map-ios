//
//  Ships.swift
//  MapPlus
//
//  Created by Toremurat Zholayev on 17.03.17.
//  Copyright © 2017 Zholayev Toremurat. All rights reserved.
//

import Foundation
import SwiftyJSON
struct Ships {
    var id:Int
    var from:Locations
    var to:Locations
    var price:Int
    var provisionalDate:String
    var desc:String
    var volume:Int
    var capacity:Int
    var insured:String
    var typeTransp:String
    var preferredCar:String
    var hazardClass:String
    var termsPayment:String
    var loadingOption:String
    var distance:Int
    var companyName:String
    var status:Int
    
    init?(json:JSON?){
        guard let json = json, let price = json["price"].int,
        let provisionalDate = json["provisionalDate"].string,
        let desc = json["description"].string,
        let volume = json["volume"].int,
        let capacity = json["capacity"].int,
        let insured = json["insured"].string,
        let typeTransp = json["typeTransp"].string,
        let preferredCar = json["preferredCar"].string,
        let hazardClass = json["hazardClass"].string,
        let termsPayment = json["termsPayment"].string,
        let loadingOption = json["loadingOption"].string,
        let distance = json["location"]["distance"].int,
        let companyName = json["customer"]["name"].string,
        let status = json["status"].int,
        let id = json["id"].int
        else {
            print("json error ships 1")
            return nil
        }
        if let to = Locations(json: json["location"]["to"]), let from = Locations(json: json["location"]["from"]) {
            self.id = id
            self.from = from
            self.to = to
            self.price = price
            self.provisionalDate = provisionalDate
            self.desc = desc
            self.volume = volume
            self.capacity = capacity
            self.insured = insured
            self.typeTransp = typeTransp
            self.preferredCar = preferredCar
            self.hazardClass = hazardClass
            self.termsPayment = termsPayment
            self.loadingOption = loadingOption
            self.distance = distance
            self.companyName = companyName
            self.status = status
            
        }
        else {
            print("json error ships 2")
            return nil
        }
    }
    
}
