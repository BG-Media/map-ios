//
//  GoodsViewController.swift
//  MapPlus
//
//  Created by Toremurat Zholayev on 10.01.17.
//  Copyright © 2017 Zholayev Toremurat. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD
class GoodsViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout  {
    private var myCars: [MyCars] = []
    private var results: [Ships] = []
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh(sender:)), for: UIControlEvents.valueChanged)
        
        return refreshControl
    }()
    var isTransport = false
    var addButton:UIButton?
    @IBOutlet weak var collectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        if isTransport {
            setupAddButton()
            getMyTransport()
        }
        collectionView.addSubview(refreshControl)
        collectionView.delegate = self
        collectionView.dataSource = self
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 1
        collectionView!.collectionViewLayout = layout
        collectionView.register(UINib.init(nibName: "GoodsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "Good")
        
        
      
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(GoodsViewController.catchNotification),
                                               name: NSNotification.Name(rawValue: "UpdateTransport"),
                                               object: nil)
       
        
    }
    func catchNotification(notification:NSNotification) -> Void {
        guard let userInfo = notification.userInfo, let isNeedUpdate  = userInfo["isNeedUpdate"] as? Bool else {
            return
        }
        if isNeedUpdate {
            let alert = UIAlertController(title: "Выполнено", message: "Транспорт успешно добавлен!", preferredStyle: .alert)
            let cancelButton = UIAlertAction(title: "Закрыть", style: .cancel, handler: nil)
            alert.addAction(cancelButton)
            self.present(alert, animated: true, completion: nil)
            
            getMyTransport()
        }
    
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if !isTransport{
            getMyOrders()
        }
    
    }
    
    //Refresh Control
    func refresh(sender:AnyObject) {
        refreshBegin { (x:Int) in
            //updateHere
            
            self.refreshControl.endRefreshing()
        }
    }
    func refreshBegin(refreshEnd:@escaping (Int) -> ()) {
        DispatchQueue.global().async {
            print("refreshing")
            self.isTransport ? (self.getMyTransport()) : (self.getMyOrders())
            sleep(2)
            
            DispatchQueue.main.async {
                refreshEnd(0)
                
            }
        }
    }
    
    
    func setupAddButton(){
        addButton = UIButton()
        let button = addButton!
        button.setTitle("Добавить транспорт", for: .normal)
        button.backgroundColor = UIColor(red:0.15, green:0.57, blue:0.87, alpha:1.0)
        button.tintColor = UIColor.white
        button.titleLabel?.font = UIFont.systemFont(ofSize: 16.0)
        button.layer.cornerRadius = 15.0
        button.addTarget(self, action: #selector(GoodsViewController.addButtonPressed), for: .touchUpInside)
        view.addSubview(button)
        button.isHidden = false
        button.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.bottom.equalToSuperview().offset(44)
            make.height.equalTo(44)
            make.width.equalTo(200)
        }
       
        addButton!.transform = CGAffineTransform(rotationAngle: 0)
        delay(seconds: 0.4, completion: {
            
            //
            // not meant to be precise like newton's law
            UIView.animate(withDuration: 0.5,
                           delay: 0.0,
                           options: .curveEaseOut,//CurveLinear//CurveEaseIn
                animations: {
                    // constraints don't get animated by default, so you need layoutIfNeeded()
                    self.addButton!.snp.makeConstraints({ (make) in
                        make.bottom.equalToSuperview().offset(-20)
                    })
                    self.view.layoutIfNeeded()
                    // flip the burger
                    
            },
                completion: nil
            )
        })
         self.view.layoutIfNeeded()

        

    }
    func delay(seconds: Double, completion:@escaping ()->()) {
        DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
            completion()
        }
    }
    // MARK: ---------------------------ADD BUTTON ACTION
    func addButtonPressed(sender:UIButton){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        //let vc = storyboard.instantiateViewController(withIdentifier: "PageNC") as! UINavigationController
        let pc = storyboard.instantiateViewController(withIdentifier: "PageVC") as! PageViewController
        pc.isOnline = true
        
        self.navigationController?.pushViewController(pc, animated: true)
    }
    
    // MARK: ---------------------------COLLECTION VIEW
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Good", for: indexPath as IndexPath) as! GoodsCollectionViewCell
        if isTransport {
            cell.carNameLabel.text = "\(myCars[indexPath.row].car.brand_name) \(myCars[indexPath.row].car.model_name)"
            cell.carDescLabel.text = "\(myCars[indexPath.row].year) · \(myCars[indexPath.row].car.type) · \(myCars[indexPath.row].volume) м³ · \(myCars[indexPath.row].capacity) т "
            cell.carVolumeLabel.text = "Загружено: \(myCars[indexPath.row].using.volume) м³ - \(myCars[indexPath.row].using.capacity) т    Свободно: \(myCars[indexPath.row].free.volume) м³ - \(myCars[indexPath.row].free.capacity) т"
            cell.priceLabel.text = ""

            cell.carStatusLabel.text = getStatusName(id: myCars[indexPath.row].status)
            cell.carVolumeLabel.textColor = UIColor.black
            
        }
        else {
            cell.carNameLabel.text = "\(results[indexPath.row].from.titleRu) → \(results[indexPath.row].to.titleRu)"
            cell.carDescLabel.text = "\(results[indexPath.row].provisionalDate) · \(results[indexPath.row].distance) · \(results[indexPath.row].volume) м³ · \(results[indexPath.row].capacity) т "
            cell.carVolumeLabel.text = results[indexPath.row].companyName
            cell.priceLabel.text = "\(results[indexPath.row].price)"
            cell.carStatusLabel.text = self.getStatusName(id: results[indexPath.row].status)
            cell.carVolumeLabel.textColor = UIColor.black
        }
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if isTransport {
            self.performSegue(withIdentifier: "SegueCar", sender: myCars[indexPath.row])
        }
        else {
            self.performSegue(withIdentifier: "SegueDetail", sender: results[indexPath.row])
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if isTransport {
            return myCars.count
        }
        
        return results.count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 95)
    }
    
    
    // MARK: ---------------------------PREPARE FOR SEGUE
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SegueCar" {
            if let vc = segue.destination as? DetailTransportViewController {
                if let snd = sender as? MyCars {
                    vc.carDetail = snd
                }
            }
        }
        if segue.identifier == "SegueDetail" {
            if let vc = segue.destination as? DetailGoodsViewController {
                if let snd = sender as? Ships {
                    vc.detail = snd
                    vc.isMyShips = true
                }
            }
        }
    }
    

    func getMyTransport() {
        SVProgressHUD.show()
        Alamofire.request(BaseUrl.mainUrl + BaseUrl.myCars, method: .post,
            parameters: ["device_token": BaseUrl.token]).responseJSON { [weak self] response in
                switch response.result {
                case .success:
                    guard let vc = self, let data = response.data else {return }
                    let json = JSON(data:data)
                    print(json)
                    vc.myCars.removeAll()
                    if json["status"] == "success" {
                        for (_, subJson):(String, JSON) in json["result"] {
                            if let newCar = MyCars(json: subJson) {
                                vc.myCars.append(newCar)
                            }
                        }
                        SVProgressHUD.dismiss()
                        vc.collectionView.reloadData()
                    }
                case .failure(let error):
                    print(error)
                    SVProgressHUD.showError(withStatus: error.localizedDescription)
                    
                }
        }

    }
    
    //
    func getMyOrders(){
        SVProgressHUD.show()
        Alamofire.request(BaseUrl.mainUrl + BaseUrl.myOrders, method: .post,
                          parameters: ["device_token": BaseUrl.token]).responseJSON { [weak self] response in
                            switch response.result {
                            case .success:
                                guard let vc = self, let data = response.data else {return }
                                let json = JSON(data:data)
                                print(json)
                                
                                if json["status"] == "success" {
                                    if json["result"].count == 0 {
                                        vc.showEmptyData(title: "У вас еще нет принятых грузов")
                                    }
                                    vc.results.removeAll()
                                    for (_, subJson):(String, JSON) in json["result"] {
                                        if let new = Ships(json: subJson) {
                                            vc.results.append(new)
                                        }
                                    }
                                    SVProgressHUD.dismiss()
                                    vc.collectionView.reloadData()
                                }
                                else if json["status"] == "error" {
                                    SVProgressHUD.showError(withStatus: json["result"]["errors"].string)
                                }
                            case .failure(let error):
                                print(error)
                                SVProgressHUD.showError(withStatus: error.localizedDescription)
                                
                            }
        }

    }
   
}
extension UIViewController {
    func showEmptyData(title:String) {
        let label = UILabel()
        let image = UIImageView()
        label.font = UIFont.systemFont(ofSize: 17.0)
        label.textColor = UIColor.gray
        label.textAlignment = .center
        label.numberOfLines = 3
        label.text = title
        image.image = UIImage(named: "dataSheet")
        view.addSubview(label)
        view.addSubview(image)
       
        image.snp.makeConstraints { (make) in
            make.width.equalTo(70)
            make.height.equalTo(70)
            make.centerX.equalToSuperview()
            make.top.equalToSuperview().offset(100)
            
            
        }
        label.snp.makeConstraints { (make) in
            make.top.equalTo(image.snp.bottom).offset(16)
            make.left.equalToSuperview().offset(16)
            make.right.equalToSuperview().offset(-16)
            
            
        }
        
    }
    func getStatusName(id:Int)->String {
        switch id {
        case 1:
            return "Принят"
        case 2:
            return "Отказан"
        case 3:
            return "Доставлен"
        case 4:
            return "Завершен"
        default:
            return ""
        }
    }
}
