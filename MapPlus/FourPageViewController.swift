//
//  FourPageViewController.swift
//  MapPlus
//
//  Created by Toremurat Zholayev on 03.02.17.
//  Copyright © 2017 Zholayev Toremurat. All rights reserved.
//

import UIKit

class FourPageViewController: UIViewController {
    private var loginTextField:UITextField!
    private var passwordTextField:UITextField!
    private var buttonNext:UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        if let email = UserDefaults.standard.value(forKey: "email") as? String, let pass = UserDefaults.standard.value(forKey: "password") as? String {
            loginTextField.text = email
            passwordTextField.text = pass
        }
    }
    func setupView(){
        
        // Login Text Field 
        
        loginTextField = UITextField()
        let placeholder = NSAttributedString(string: "Логин", attributes: [NSForegroundColorAttributeName:UIColor(red:0, green:0, blue:0, alpha:0.4)])
        loginTextField.attributedPlaceholder = placeholder
        loginTextField.clearsOnBeginEditing = true
        loginTextField.borderStyle = UITextBorderStyle.none
        loginTextField.font = UIFont.systemFont(ofSize: 17.0)
        loginTextField.spellCheckingType = .no
        loginTextField.autocorrectionType  = .no
        view.addSubview(loginTextField)
        
        loginTextField.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(16)
            make.top.equalToSuperview().offset(33)
            make.right.equalToSuperview().offset(-16)
            make.height.equalTo(20)
            
        }
        let line1: UIView = {
            let sep = UIView()
            sep.backgroundColor = UIColor(red:0, green:0, blue:0, alpha:0.1)
            return sep
        }()
        view.addSubview(line1)
        line1.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(16)
            make.right.equalToSuperview().offset(-16)
            make.top.equalTo(loginTextField.snp.bottom).offset(10)
            make.height.equalTo(1)
        }
        
        //Password Text Field
        passwordTextField = UITextField()
        let placeholderPass = NSAttributedString(string: "Пароль", attributes: [NSForegroundColorAttributeName:UIColor(red:0, green:0, blue:0, alpha:0.4)])
        passwordTextField.attributedPlaceholder = placeholderPass
        passwordTextField.clearsOnBeginEditing = true
        passwordTextField.borderStyle = UITextBorderStyle.none
        passwordTextField.font = UIFont.systemFont(ofSize: 17.0)
        passwordTextField.isSecureTextEntry = true
        view.addSubview(passwordTextField)
        
        passwordTextField.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(16)
            make.top.equalTo(line1.snp.bottom).offset(18)
            make.right.equalToSuperview().offset(-16)
            make.height.equalTo(40)
        }
        let line2: UIView = {
            let sep = UIView()
            sep.backgroundColor = UIColor(red:0, green:0, blue:0, alpha:0.1)
            return sep
        }()
        view.addSubview(line2)
        line2.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(16)
            make.right.equalToSuperview().offset(-16)
            make.top.equalTo(passwordTextField.snp.bottom).offset(10)
            make.height.equalTo(1)
        }
        
        //Button Next
        
        buttonNext = UIButton()
        buttonNext.setTitle("Далее", for: .normal)
        buttonNext.backgroundColor = UIColor(red:0.12, green:0.70, blue:1.00, alpha:1.0)
        buttonNext.setTitleColor(UIColor.white, for: .normal)
        buttonNext.titleLabel?.font = UIFont.systemFont(ofSize: 17.0)
        buttonNext.addTarget(self, action: #selector(FourPageViewController.nextButtonPressed), for: .touchUpInside)
        view.addSubview(buttonNext)
        buttonNext.snp.makeConstraints { (make) in
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.top.equalTo(line2.snp.bottom).offset(32)
            make.height.equalTo(40)
            
            
        }
        
    }
    func nextButtonPressed() {
        guard let email = loginTextField.text, !email.isEmpty, let password = passwordTextField.text, !password.isEmpty else {
            print("please fill")
            return
        }
        let user = User(email: email, password: password, token: BaseUrl.token)
        user.loginUser { () in
            UserDefaults.standard.removeObject(forKey: "password")
            UserDefaults.standard.removeObject(forKey: "email")
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "TabBarVC") as! UITabBarController
            self.present(vc, animated: true, completion: nil)
        }
        
    }

}
