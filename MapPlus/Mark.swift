//
//  Mark.swift
//  MapPlus
//
//  Created by Toremurat Zholayev on 14.02.17.
//  Copyright © 2017 Zholayev Toremurat. All rights reserved.
//

import Foundation
import SwiftyJSON
struct Mark {
    var id:Int
    var name:String
    var models:[Model]
    
    init?(json: JSON?) {

        guard let json = json, let id = json["id"].int, let name = json["name_ru"].string else{
            print("json err")
            return nil
        }
        var newModels:[Model] = []
        for (_, subJson):(String, JSON) in json["models"] {
            if let model = Model(json: subJson) {
                newModels.append(model)
            }
        }
        self.id = id
        self.name = name
        self.models = newModels
    }
    func numberOfModels() -> Int {
        return models.count
    }
}
