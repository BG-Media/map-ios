//
//  FilterDataTableViewController.swift
//  MapPlus
//
//  Created by Toremurat Zholayev on 15.03.17.
//  Copyright © 2017 Zholayev Toremurat. All rights reserved.
//

import UIKit
protocol FilterDelegate {
    func getLocations(detail: Locations, indexPath:Int)
    func getTypes(detail:Type, indexPath:Int)
    func getStatuses(detail:String, indexPath:Int)
}
class FilterDataTableViewController: UITableViewController {
    
    var locations:[Locations]!
    var types:[Type]!
    var statutes:[String]!
    var typeId = 0
    var delegate: FilterDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        
      
    }

    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if typeId == 0 || typeId == 1 {
            return locations.count
        }
        else if typeId == 3 || typeId == 4 || typeId == 8 || typeId == 9 {
            return types.count
        }
        else if typeId == 2 {
            return statutes.count
        }
        else {
            return 0
        }
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: "Cell")
        if typeId == 0 || typeId == 1{
             cell.textLabel?.text = locations[indexPath.row].addressRu
        }
        else if typeId == 3 || typeId == 4 || typeId == 8 || typeId == 9 {
            cell.textLabel?.text = types[indexPath.row].name
        }
        else if typeId == 2 {
            cell.textLabel?.text = statutes[indexPath.row]
        }
       
        return cell
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if typeId == 0  || typeId == 1 {
            delegate?.getLocations(detail: locations[indexPath.row], indexPath: typeId)
        }
        else if typeId == 3 || typeId == 4 || typeId == 8 || typeId == 9{
            delegate?.getTypes(detail: types[indexPath.row], indexPath: typeId)
        }
        else if typeId == 2 {
            delegate?.getStatuses(detail: statutes[indexPath.row], indexPath: typeId)
        }
        
        self.navigationController?.popViewController(animated: true)
    }
    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
