//
//  ThirdPagveViewController.swift
//  MapPlus
//
//  Created by Toremurat Zholayev on 03.02.17.
//  Copyright © 2017 Zholayev Toremurat. All rights reserved.
//

import UIKit
import SnapKit
import Alamofire
import SVProgressHUD
let cellIdentifier = "Cell"
class ThirdPageViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    let picker = UIImagePickerController()
      var isOnline = false
    var images: [UIImage] = []{
  
        didSet {
            if images.count > 6 {
                
            }
        }
    }
    
    @IBOutlet weak var collectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.delegate = self
        collectionView.dataSource = self
        picker.delegate = self
        
        let layout:UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0)
        collectionView.collectionViewLayout = layout
        collectionView.register(ImageCell.self, forCellWithReuseIdentifier: cellIdentifier)
        self.hideKeyboardWhenTappedAround()
        // Do any additional setup after loading the view.
      
    }
  
  
  
    //MARK: - Photo Picker Delegates
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage //2
        
        let imageData:Data = UIImagePNGRepresentation(chosenImage)!
       

        if let car_id = UserDefaults.standard.value(forKey: "car_id") as? Int {
            uploadImageBy(id: "\(car_id)", image: chosenImage)
        }
        dismiss(animated:true, completion: nil) //5
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
         dismiss(animated: true, completion: nil)
    }
  
  
    //MARK: - Collection View Delegates
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 6
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! ImageCell
        
        cell.photoView.image = images.count - 1 >= indexPath.row && !images.isEmpty ? images[indexPath.row] : UIImage(named: "placeholder")
        
       
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        picker.allowsEditing = false
        picker.sourceType = .photoLibrary
        picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
        present(picker, animated: true, completion: nil)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width/3 - 10 , height: collectionView.frame.height/2 - 10)
        
    }
 
    @IBAction func nextButtonPressed(_ sender: UIButton) {
        if isOnline {
//            let alert = UIAlertController(title: "Выполнено!", message: "Транспорт успешно добавлен!", preferredStyle: .alert)
//            let cancelButton = UIAlertAction(title: "Закрыть", style: .cancel, handler: nil)
//            alert.addAction(cancelButton)
//            self.present(alert, animated: true, completion: nil)
            
            self.navigationController?.popToRootViewController(animated: true)
            let nc = NotificationCenter.default
            nc.post(name: Notification.Name(rawValue: "UpdateTransport"),
                    object: nil,
                    userInfo: ["isNeedUpdate":true])
            
           // self.navigationController?.popViewController(animated: true)
        }
        else {
            self.moveToNextPageWith(index: 3, title: "Завершение добавления")
        }

    }
    
    
    func uploadImageBy(id:String, image:UIImage) {
        DispatchQueue.main.async {
            SVProgressHUD.show(withStatus: "Добавляем изображение...")
        }
        let parameters = [
            "device_token": BaseUrl.token,
            "car_id": id
        ]
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters {
                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            }
       
            if let imageData = UIImageJPEGRepresentation(image, 0.5) {
                multipartFormData.append(imageData, withName: "image", fileName: "fromIphoneImage.jpeg", mimeType: "image/jpeg")
            }
       
        }, to: BaseUrl.mainUrl + BaseUrl.addImage, method: .post) { (encodingResult) in
            
            switch encodingResult {
            
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (progress) in
           
                    SVProgressHUD.showProgress(Float(progress.fractionCompleted), status: "Загрузка")
                })
                upload.responseJSON { [weak self] response in
                guard let vc = self else {return }
                    if let json = response.result.value as? [String:AnyObject] {
                        print(json)
                        if let status = json["status"] as? String, let result = json["result"] as? [String:AnyObject] {
                            
                            if status == "success"{
                                print(result)
                                vc.images.append(image)
                                DispatchQueue.main.async {
                                    vc.collectionView.reloadData()
                                    SVProgressHUD.showSuccess(withStatus: "Изменения успешно сохранены")
                                    
                                }
                                
                                
                            }
                            else if status == "error" {
                                SVProgressHUD.dismiss()
                                
                                if let error = result["error"] as? String {
                                    SVProgressHUD.showError(withStatus: error)
                                }
                                
                                
                            }
                        }
                    }

                    
            }
            case .failure(let encodingError):
                print(encodingError)
                SVProgressHUD.showError(withStatus: encodingError.localizedDescription)
               
                
            }
           
         
            
        }
    }
    
}
class ImageCell:UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    let photoView:UIImageView = {
        let img = UIImageView()
        img.contentMode = .scaleAspectFill
        img.clipsToBounds = true
        
        img.image = UIImage(named: "placeholder")
        return img
    }()
    let addButton:UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named:"addImg"), for: .normal)
        return button
    }()
 
    
    func setupViews(){
        addSubview(photoView)
        
        addSubview(addButton)
        photoView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview().inset(UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0))
            
        }
        addButton.snp.makeConstraints { (make) in
            make.centerX.equalTo(photoView)
            make.centerY.equalTo(photoView)
            make.height.equalTo(16)
            make.width.equalTo(16)
        }
    }
    
}
