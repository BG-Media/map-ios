//
//  FilterViewController.swift
//  MapPlus
//
//  Created by Toremurat Zholayev on 15.03.17.
//  Copyright © 2017 Zholayev Toremurat. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD
import SwiftyJSON
protocol FilterResultDelegate {
    func getFilterList(results:[Ships])
    
}

class FilterViewController: UITableViewController, FilterDelegate {

    @IBOutlet weak var priceMax: UITextField!
    @IBOutlet weak var priceMin: UITextField!
    @IBOutlet weak var capacityMax: UITextField!
    @IBOutlet weak var capacityMin: UITextField!
    @IBOutlet weak var volumeMin: UITextField!
    @IBOutlet weak var volumeMax: UITextField!
    @IBOutlet weak var dangerLabel: UILabel!
    @IBOutlet weak var transportLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var insuranceLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var cityToLabel: UILabel!
    @IBOutlet weak var cityFromLabel: UILabel!
   
   
    var delegate: FilterResultDelegate?
    var filters:Filter?
    var pointA:Int?
    var pointB:Int?
    var filterData = ["","","",""]
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Фильтр"
        tableView.isUserInteractionEnabled = false
        getFilterParams(token: BaseUrl.token)
    }
    //Filter protocol delegates 
    @IBAction func showResultsButtonPressed(_ sender: UIButton) {
        if let first = pointA, let second = pointB {
            print(first)
            getFilterResults(pointA: first, pointB: second, insured: filterData[0], typeTransp: filterData[1], carTypes: filterData[2], hazardClass: filterData[3], capacity_min: capacityMin.text!, capacity_max: capacityMax.text!, volume_min: volumeMin.text!, volume_max: volumeMax.text!, price_min: priceMin.text!, price_max: priceMax.text!)
        }
        else {
            SVProgressHUD.showError(withStatus: "Выберите пункты отправления и назначения")

        }
        
    }
    func getLocations(detail: Locations, indexPath:Int) {
        print(detail.addressRu)
        if indexPath == 0 {
            cityFromLabel.text = detail.addressRu
            pointA = detail.map_id
        }
        if indexPath == 1 {
            cityToLabel.text = detail.addressRu
            pointB = detail.map_id
        }
    }
    func getTypes(detail: Type, indexPath:Int) {
        print(detail.name)
        if indexPath == 3 {
            insuranceLabel.text = detail.name
            filterData[0] = "\(detail.id)"
        }
        if indexPath == 4 {
            typeLabel.text = detail.name
            filterData[1] = "\(detail.id)"
        }
        if indexPath == 8 {
            transportLabel.text = detail.name
            filterData[2] = "\(detail.id)"
        }
        if indexPath == 9 {
            dangerLabel.text = detail.name
            filterData[3] = "\(detail.id)"
        }
    }
    func getStatuses(detail: String, indexPath:Int) {
        print(detail)
        statusLabel.text = detail
        
    }
    
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 11
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let vc = FilterDataTableViewController()
        vc.delegate = self
        if indexPath.row == 0 {
            vc.locations = filters?.locations
            vc.typeId = indexPath.row
            self.navigationController?.pushViewController(vc, animated: true)
        }
        if indexPath.row == 1 {
            vc.locations = filters?.locations
            vc.typeId = indexPath.row
            self.navigationController?.pushViewController(vc, animated: true)
        }
        if indexPath.row == 2 {
            vc.statutes = filters?.statuses
            vc.typeId = indexPath.row
            self.navigationController?.pushViewController(vc, animated: true)
        }
        if indexPath.row == 3 {
            vc.types = filters?.insured
            vc.typeId = indexPath.row
            self.navigationController?.pushViewController(vc, animated: true)
        }
        if indexPath.row == 4 {
            vc.types = filters?.typeTransp
            vc.typeId = indexPath.row
            self.navigationController?.pushViewController(vc, animated: true)

        }
        if indexPath.row == 8 {
            vc.types = filters?.carTypes
            vc.typeId = indexPath.row
            self.navigationController?.pushViewController(vc, animated: true)

        }
        if indexPath.row == 9 {
            vc.types = filters?.hazardClass
            vc.typeId = indexPath.row
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        if indexPath.row == 10 {
            
           // getFilterResults(pointA: , pointB: <#T##String#>, insured: <#T##String#>, typeTransp: <#T##String#>, carTypes: <#T##Int#>, hazardClass: <#T##Int#>, capacity_min: <#T##Int#>, capacity_max: <#T##Int#>, volume_min: <#T##Int#>, volume_max: <#T##Int#>, price_min: <#T##Int#>, price_max: <#T##Int#>)
        }
        
    }
    //http://mapplus.bugingroup.kz/api/v1/transporter/filter/preLoad
    func getFilterParams(token:String) {
        SVProgressHUD.show()
        Alamofire.request(BaseUrl.mainUrl + BaseUrl.filterParams, method: .post,
                          parameters: ["device_token":token]).responseJSON { [weak self] response in
                            switch response.result {
                            case .success:
                                guard let vc = self, let data = response.data else {return }
                                let json = JSON(data:data)
                                print(json)
                                if json["status"] == "success" {
                                    if let filter = Filter(json: json["result"]) {
                                        vc.filters = filter
                                        vc.tableView.isUserInteractionEnabled = true
                                        SVProgressHUD.dismiss()
                                    }
                                    
                                    
                                }
                            case .failure(let error):
                                print(error)
                                SVProgressHUD.showError(withStatus: error.localizedDescription)
                                
                            }
        }
        
    }
    func getFilterResults(pointA:Int, pointB:Int, insured:String,typeTransp:String, carTypes:String,hazardClass:String, capacity_min:String,capacity_max:String, volume_min:String, volume_max:String, price_min:String, price_max:String){
        SVProgressHUD.show()
        Alamofire.request(BaseUrl.mainUrl + BaseUrl.getFilterList, method: .post, parameters:
            ["device_token": BaseUrl.token,
             "pointA":pointA,
             "pointB":pointB,
             "insured":insured,
             "typeTransp":typeTransp,
             "carTypes":carTypes,
             "hazardClass":hazardClass,
             "capacity_min":capacity_min,
             "capacity_max":capacity_max,
             "volume_min":volume_min,
             "volume_max":volume_max,
             "price_min":price_min,
             "price_max":price_max
             ]).responseJSON { [weak self] response in
                            switch response.result {
                            case .success:
                                guard let vc = self, let data = response.data else {return }
                                let json = JSON(data:data)
                                print(json)
                                 var resultList: [Ships] = []
                                if json["status"] == "success" {
                                    for (_, subJson):(String, JSON) in json["result"] {
                                        if let new = Ships(json: subJson) {
                                            resultList.append(new)
                                        }
                                    }
                                    SVProgressHUD.dismiss()
                                    vc.delegate?.getFilterList(results: resultList)
                                    self?.navigationController?.popViewController(animated: true)
                                    
                                }
                            case .failure(let error):
                                print(error)
                                SVProgressHUD.showError(withStatus: error.localizedDescription)
                                
                            }
        }
    }
    
    
    
  
    

   

}
