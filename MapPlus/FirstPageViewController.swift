//
//  FirstPageViewController.swift
//  MapPlus
//
//  Created by Toremurat Zholayev on 27.01.17.
//  Copyright © 2017 Zholayev Toremurat. All rights reserved.
//

import UIKit
import SnapKit
import SkyFloatingLabelTextField
class FirstPageViewController: UIViewController, UITextFieldDelegate {
    var userInfo = Profile()
    var pageMenu:CAPSPageMenu?
    var buttonName = "Далее"
    var scrollView: UIScrollView!
    private var organisation:SkyFloatingLabelTextField!
    private var bin:SkyFloatingLabelTextField!
    private var phone:SkyFloatingLabelTextField!
    private var email:SkyFloatingLabelTextField!
    private var urAddress:SkyFloatingLabelTextField!
    private var factAddress:SkyFloatingLabelTextField!
    private var buttonNext:UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
       
        setupViews()
       
        if buttonName == "Сохранить" {
            organisation.text = userInfo.name
            bin.text = userInfo.iin
            phone.text = userInfo.phone
            //email.text = userInfo.email
            urAddress.text = userInfo.legal_address
            factAddress.text = userInfo.actual_address
            
        }
        phone.delegate = self
        self.hideKeyboardWhenTappedAround()
        
    }
    
    
    override func viewDidLayoutSubviews() {
         //scrollView.contentSize = CGSize(width:self.view.frame.width, height: 700)
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField == self.phone) {

            let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            if (newString.characters.count < (textField.text?.characters.count)! && newString.characters.count >= 1) {
                return true                                                         // return true for backspace to work
            } else if (newString.characters.count < 1) {
                return false;                        // deleting "+" makes no sence
            }
            if (newString.characters.count > 17 ) {
                return false;
            }
            let components = newString.components(separatedBy: NSCharacterSet.decimalDigits.inverted)
            let decimalString = components.joined(separator:
                "") as NSString
            let length = decimalString.length
            
            var index = 0
            let formattedString = NSMutableString()
            formattedString.append("+")
            
            if (length >= 1) {
                let countryCode = decimalString.substring(with: NSMakeRange(0, 1))
                formattedString.append(countryCode)
                index += 1
            }
            
            if (length > 1) {
                var rangeLength = 3
                if (length < 4) {
                    rangeLength = length - 1
                }
                let operatorCode = decimalString.substring(with: NSMakeRange(1, rangeLength))
                formattedString.appendFormat(" (%@) ", operatorCode)
                index += operatorCode.characters.count
            }
            
            if (length > 4) {
                var rangeLength = 3
                if (length < 7) {
                    rangeLength = length - 4
                }
                let prefix = decimalString.substring(with: NSMakeRange(4, rangeLength))
                formattedString.appendFormat("%@-", prefix)
                index += prefix.characters.count
            }
            
            if (index < length) {
                let remainder = decimalString.substring(from: index)
                formattedString.append(remainder)
            }
            
            textField.text = formattedString as String
            
            if (newString.characters.count == 17) {
                textField.resignFirstResponder()
            }
            
            return false
        }

        return true
    }
    // MARK: ---------------------------SETUP BASIC VIEW
    func setupViews() {
        //ScrollView
        scrollView = UIScrollView(frame: view.bounds)
        scrollView.backgroundColor = UIColor.white
        scrollView.autoresizingMask = UIViewAutoresizing.flexibleHeight
        scrollView.showsVerticalScrollIndicator = false
        //ContentView
        let contentView = UIView()
        contentView.backgroundColor = UIColor.white
        
        view.addSubview(scrollView)
        scrollView.addSubview(contentView)
        
        scrollView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview().inset(UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0))
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview()
        }
        contentView.snp.makeConstraints { (make) in
            make.centerX.equalTo(scrollView)
            make.edges.equalTo(scrollView).inset(UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0))
            make.height.equalTo(view.frame.height + 30)
            
        }
        //Elements of Content View
        
        let lightGray = UIColor(red:0, green:0, blue:0, alpha:0.1)
        let pcColor = UIColor(red:0.13, green:0.63, blue:1.00, alpha:1.0)
        organisation = SkyFloatingLabelTextField()
        let placeholder = NSAttributedString(string: "Наименование организации/ФЛ", attributes: [NSForegroundColorAttributeName:UIColor(red:0, green:0, blue:0, alpha:0.4)])
        organisation.attributedPlaceholder = placeholder
        organisation.autocorrectionType = .no
        organisation.keyboardAppearance = .dark
        organisation.title = "Наименование организации/ФЛ"
        organisation.clearsOnBeginEditing = true
        organisation.borderStyle = UITextBorderStyle.none
        organisation.font = UIFont.systemFont(ofSize: 17.0)
        organisation.lineColor = lightGray
        organisation.selectedLineColor = pcColor
        organisation.selectedLineHeight = 2.0
        organisation.selectedTitleColor = pcColor
        contentView.addSubview(organisation)
        
        organisation.snp.makeConstraints { (make) in
            make.left.equalTo(contentView).offset(16)
            make.top.equalTo(contentView).offset(18)
            make.right.equalTo(contentView).offset(-16)
            make.height.equalTo(50)
            
        }
        //Second Text Field - IIN and Bin number
        bin = SkyFloatingLabelTextField()
        let placeholderBin = NSAttributedString(string: "БИН/ИИН", attributes: [NSForegroundColorAttributeName:UIColor(red:0, green:0, blue:0, alpha:0.4)])
        bin.attributedPlaceholder = placeholderBin
        bin.title = "БИН/ИИН"
        bin.keyboardAppearance = .dark
        bin.autocorrectionType = .no
        bin.clearsOnBeginEditing = true
        bin.borderStyle = UITextBorderStyle.none
        bin.keyboardType = UIKeyboardType.numberPad
        bin.font = UIFont.systemFont(ofSize: 17.0)
        bin.lineColor = lightGray
        bin.selectedLineColor = pcColor
        bin.selectedLineHeight = 2.0
        bin.selectedTitleColor = pcColor
        contentView.addSubview(bin)
        
        bin.snp.makeConstraints { (make) in
            make.left.equalTo(contentView).offset(16)
            make.top.equalTo(organisation.snp.bottom).offset(10)
            make.right.equalTo(contentView).offset(-16)
            make.height.equalTo(50)
        }
   
        //Third Text Filed - Phone number
        phone = SkyFloatingLabelTextField()
        phone.autocorrectionType = .no
        let placeholderPhone = NSAttributedString(string: "Номер телефона", attributes: [NSForegroundColorAttributeName:UIColor(red:0, green:0, blue:0, alpha:0.4)])
        phone.attributedPlaceholder = placeholderPhone
        phone.clearsOnBeginEditing = true
        phone.title = "Номер телефона"
        phone.borderStyle = UITextBorderStyle.none
        phone.keyboardType = UIKeyboardType.numberPad
        phone.font = UIFont.systemFont(ofSize: 17.0)
        phone.lineColor = lightGray
        phone.keyboardAppearance = .dark
        phone.selectedLineColor = pcColor
        phone.selectedLineHeight = 2.0
        phone.selectedTitleColor = pcColor
        contentView.addSubview(phone)
        
        phone.snp.makeConstraints { (make) in
            make.left.equalTo(contentView).offset(16)
            make.top.equalTo(bin.snp.bottom).offset(10)
            make.right.equalTo(contentView).offset(-16)
            make.height.equalTo(50)
        }
   
        //Fourth Text Filed - Legal Address number
        urAddress = SkyFloatingLabelTextField()
        let placeholderUr = NSAttributedString(string: "Юридический адрес", attributes: [NSForegroundColorAttributeName:UIColor(red:0, green:0, blue:0, alpha:0.4)])
        urAddress.title = "Юридический адрес"
        urAddress.attributedPlaceholder = placeholderUr
        urAddress.clearsOnBeginEditing = true
        urAddress.borderStyle = UITextBorderStyle.none
        urAddress.font = UIFont.systemFont(ofSize: 17.0)
        urAddress.lineColor = lightGray
        urAddress.selectedLineColor = pcColor
        urAddress.selectedLineHeight = 2.0
        urAddress.keyboardAppearance = .dark
        urAddress.autocorrectionType = .no
        urAddress.selectedTitleColor = pcColor
        contentView.addSubview(urAddress)
        
        urAddress.snp.makeConstraints { (make) in
            make.left.equalTo(contentView).offset(16)
            make.top.equalTo(phone.snp.bottom).offset(10)
            make.right.equalTo(contentView).offset(-16)
            make.height.equalTo(50)
        }
       
        //Fourth Text Filed - Legal Address number
        factAddress = SkyFloatingLabelTextField()
        let placeholderFact = NSAttributedString(string: "Фактический адрес", attributes: [NSForegroundColorAttributeName:UIColor(red:0, green:0, blue:0, alpha:0.4)])
        factAddress.title = "Фактический адрес"
        factAddress.attributedPlaceholder = placeholderFact
        factAddress.clearsOnBeginEditing = true
        factAddress.borderStyle = UITextBorderStyle.none
        factAddress.font = UIFont.systemFont(ofSize: 17.0)
        factAddress.lineColor = lightGray
        factAddress.autocorrectionType = .no
        factAddress.keyboardAppearance = .dark
        factAddress.selectedLineColor = pcColor
        factAddress.selectedLineHeight = 2.0
        factAddress.selectedTitleColor = pcColor
        contentView.addSubview(factAddress)
        
        factAddress.snp.makeConstraints { (make) in
            make.left.equalTo(contentView).offset(16)
            make.top.equalTo(urAddress.snp.bottom).offset(10)
            make.right.equalTo(contentView).offset(-16)
            make.height.equalTo(50)
        }
       
        
        //Button Next
        buttonNext = UIButton()
        buttonNext.setTitle(buttonName, for: .normal)
        buttonNext.backgroundColor = UIColor(red:0.12, green:0.70, blue:1.00, alpha:1.0)
        buttonNext.setTitleColor(UIColor.white, for: .normal)
        buttonNext.addTarget(self, action: #selector(FirstPageViewController.nextButtonPressed), for: .touchUpInside)
        buttonNext.titleLabel?.font = UIFont.systemFont(ofSize: 17.0)
        contentView.addSubview(buttonNext)
        buttonNext.snp.makeConstraints { (make) in
            make.left.equalTo(contentView)
            make.right.equalTo(contentView)
            make.top.equalTo(factAddress.snp.bottom).offset(32)
            make.height.equalTo(40)
            
            
        }
        
        
        
    }
  
    func nextButtonPressed() {
        
        guard let name = organisation.text, !name.isEmpty, let bin = bin.text, !bin.isEmpty, let phone = phone.text, !phone.isEmpty, let urAddress = urAddress.text, !urAddress.isEmpty, let factAddress = factAddress.text, !factAddress.isEmpty else {
            print("please fill everethx")
            
            return
        }
        
        if let email = UserDefaults.standard.value(forKey: "email") as? String {
            let profile = Profile(device_token: BaseUrl.token, name: name, iin: bin, phone: phone, email: email, legal_address: urAddress, actual_address: factAddress, sphere_id: 1)
            profile.updateProfile { () in
                if self.buttonName == "Сохранить" {
                    self.navigationController?.popViewController(animated: true)
                }
                else {
                    self.moveToNextPageWith(index: 1, title: "Транспортное средство")
                }
            }
        }
       
    }
    
}

