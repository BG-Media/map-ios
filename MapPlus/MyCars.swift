//
//  MyCars.swift
//  MapPlus
//
//  Created by Toremurat Zholayev on 16.02.17.
//  Copyright © 2017 Zholayev Toremurat. All rights reserved.
//

import Foundation
import SwiftyJSON
struct carData {
    var brand_name: String
    var model_name: String
    var type: String
    
    init?(json:JSON?) {
        guard let json = json,
        let brand_name = json["brand_name"].string,
        let model_name = json["model_name"].string,
        let type = json["type"].string else {
            return nil
        }
        self.brand_name = brand_name
        self.model_name = model_name
        self.type = type
        
    }
   
}
struct Media {
    var id: Int
    var entity_id: Int
    var path: String
    
    init?(json:JSON?) {
        guard let json = json,
            let id = json["id"].int,
            let entity_id = json["entity_id"].int,
            let path = json["path"].string else {
                return nil
        }
        self.id = id
        self.entity_id = entity_id
        self.path = path
        
    }
}
struct shipData {
    var capacity: Int
    var volume: Int
    init?(json:JSON?) {
        guard let json = json,
        let capacity = json["capacity"].int,
        let volume = json["volume"].int else {
            return nil
        }
        self.capacity = capacity
        self.volume = volume
    }
}
struct MyCars {
    var id:Int
    var user_id: Int
    var brand: Int
    var model: Int
    var year: Int
    var type: Int
    var volume: Int
    var capacity: Int
    var gov_number: String
    var reg_place: String
    var date_tech_inspection: String
    var insurance_policy: String
    var desc: String
    var status: Int
    var time_history: Int
    var distance_history: Int
    var car: carData
    var using: shipData
    var free: shipData
    var media : [Media]
    init? (json:JSON?) {
        guard let json = json,
            let id = json["id"].int,
            let user_id = json["user_id"].int,
            let brand = json["brand"].int,
            let model = json["model"].int,
            let year = json["year"].int,
            let type = json["type"].int,
            let volume = json["volume"].int,
            let capacity = json["capacity"].int,
            let gov_number = json["gov_number"].string,
            let reg_place = json["reg_place"].string,
            let date_tech_inspection = json["date_tech_inspection"].string,
            let insurance_policy = json["insurance_policy"].string,
            let desc = json["description"].string,
            let status = json["status"].int,
            let time_history = json["time_history"].int,
            let distance_history = json["distance_history"].int
            
        else {
            print("json failed in first parsing")
                return nil
        }
        
        guard let newCar :carData = carData(json: json["car"]), let newUsing :shipData = shipData(json: json["using"]), let newFree = shipData(json: json["free"]) else {
            print("json failed in car using free")
            return nil
        }
        var mediaArr: [Media] = []
        for (_, subJson):(String, JSON) in json["media"] {
            if let new = Media(json: subJson) {
                mediaArr.append(new)
            }
        }
        

        self.media = mediaArr
        self.id = id
        self.user_id = user_id
        self.brand = brand
        self.model = model
        self.year = year
        self.type = type
        self.volume = volume
        self.capacity = capacity
        self.gov_number = gov_number
        self.reg_place = reg_place
        self.date_tech_inspection = date_tech_inspection
        self.insurance_policy = insurance_policy
        self.desc = desc
        self.status = status
        self.time_history = time_history
        self.distance_history = distance_history
        self.car = newCar
        self.using = newUsing
        self.free = newFree
    }
}
