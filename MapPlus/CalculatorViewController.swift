//
//  CalculatorViewController.swift
//  MapPlus
//
//  Created by Toremurat Zholayev on 17.03.17.
//  Copyright © 2017 Zholayev Toremurat. All rights reserved.
//

import UIKit
import GoogleMaps
import SnapKit
import Alamofire
import SwiftyJSON
import SVProgressHUD
class CalculatorViewController: UIViewController, FilterDelegate, UIWebViewDelegate {
    var filters:Filter?

    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var calculateButton: UIButton!
    @IBOutlet weak var fromLabel: UIButton!
    @IBOutlet weak var toButton: UIButton!
    var mapV: GMSMapView!
    override func viewDidLoad() {
        super.viewDidLoad()
//        let camera = GMSCameraPosition.camera(withLatitude: 43.238286, longitude:  76.945456, zoom: 7)
//        mapV = GMSMapView.map(withFrame: CGRect.init(x: 0, y: self.view.frame.height/5, width: self.view.frame.width, height: self.view.frame.height * 0.35), camera: camera)
//        //view.addSubview(mapV)
//       
//        
//        //getFilterParams(token: BaseUrl.token)
//        
//        // Do any additional setup after loading the view.
        webView.isHidden = true
        
        webView.delegate = self
        
        if let url = URL(string: "http://mapdemo.bugingroup.kz/#/cabinet/calculator") {
            SVProgressHUD.show()
            let request = URLRequest(url: url)
            webView.loadRequest(request)
        }
        
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        let loadStyles = "var div = document.getElementsByClassName('row')[0]; div.setAttribute('style', 'display:none;'); var div2 = document.getElementsByClassName('header')[0]; div2.setAttribute('style', 'display:none;');var div3 = document.getElementsByClassName('main-title')[0]; div3.setAttribute('style', 'display:none;');var div4 = document.getElementsByClassName('other-page')[0]; div4.setAttribute('style', 'padding-top:0px;');var div4 = document.getElementsByClassName('in-about-content')[0]; div4.setAttribute('style', 'margin: 0px;');var div5 = document.getElementsByClassName('take_orders')[0]; div5.setAttribute('style', 'width: 100%;');var div6 = document.getElementsByClassName('footer')[0]; div6.setAttribute('style','display:none;');var div7 = document.getElementsByClassName('other-page')[0]; div7.setAttribute('style','padding:0;');var div8 = document.getElementsByClassName('ourFeatures ')[0]; div8.setAttribute('style','display:none;');var div9 = document.getElementsByTagName('a ')[0]; div9.setAttribute('style','display:none;');"
        print(loadStyles)
        
        webView.stringByEvaluatingJavaScript(from: loadStyles)
        
        webView.isHidden = false
        SVProgressHUD.dismiss()
    }
    
    @IBAction func calculateButtonPressed(_ sender: UIButton) {
    }
    @IBAction func fromButtonPressed(_ sender: UIButton) {
        let vc = FilterDataTableViewController()
        vc.delegate = self
        vc.locations = filters?.locations
        vc.typeId = 0
        self.navigationController?.pushViewController(vc, animated: true)

    }
    @IBAction func toButtonPressed(_ sender: UIButton) {
        let vc = FilterDataTableViewController()
        vc.delegate = self
        vc.locations = filters?.locations
        vc.typeId = 1
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func getLocations(detail: Locations, indexPath:Int) {
        print(detail.addressRu)
        if indexPath == 0 {
            fromLabel.setTitle(detail.addressRu, for: .normal)
            let position = CLLocationCoordinate2DMake(43.238286,76.945456)
            let london = GMSMarker(position: position)
            london.title = detail.titleRu
            london.snippet = detail.addressRu
            
      
            london.map = mapV
        }
        
        if indexPath == 1 {
            toButton.setTitle(detail.addressRu, for: .normal)
        }
    }
    func getTypes(detail: Type, indexPath:Int) {
        print(detail.name)
      
    }
    func getStatuses(detail: String, indexPath:Int) {
        print(detail)
        
        
    }
    
    func getFilterParams(token:String) {
        SVProgressHUD.show()
        Alamofire.request(BaseUrl.mainUrl + BaseUrl.filterParams, method: .post,
                          parameters: ["device_token":token]).responseJSON { [weak self] response in
                            switch response.result {
                            case .success:
                                guard let vc = self, let data = response.data else {return }
                                let json = JSON(data:data)
                                print(json)
                                if json["status"] == "success" {
                                    if let filter = Filter(json: json["result"]) {
                                        vc.filters = filter
                                        SVProgressHUD.dismiss()
                                        vc.toButton.isUserInteractionEnabled = true
                                        vc.fromLabel.isUserInteractionEnabled = true
                                    }
                                    
                                    
                                }
                            case .failure(let error):
                                print(error)
                                SVProgressHUD.showError(withStatus: error.localizedDescription)
                                
                            }
        }
        
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
