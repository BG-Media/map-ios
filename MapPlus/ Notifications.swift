//
//   Notifications.swift
//  MapPlus
//
//  Created by Toremurat Zholayev on 12.04.17.
//  Copyright © 2017 Zholayev Toremurat. All rights reserved.
//

import Foundation
import SwiftyJSON
struct Notifications {
    var id: Int
    var user_id: Int
    var text: String
    var type: String
    var readed: Int
    var disabled: Int
    var time: Int
    var entity_id: Int
    
    init?(json: JSON?) {
        guard let json = json, let id = json["id"].int, let user_id = json["user_id"].int, let text = json["text"].string,
            let type = json["type"].string, let readed = json["readed"].int, let disabled = json["disabled"].int,
            let time = json["time"].int, let entity_id = json["entity_id"].int else {
            return nil
        }
        self.id = id
        self.user_id = user_id
        self.text = text
        self.type = type
        self.readed = readed
        self.disabled = disabled
        self.time = time
        self.entity_id = entity_id
    }
    
}
