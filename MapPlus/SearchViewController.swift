//
//  SearchViewController.swift
//  MapPlus
//
//  Created by Toremurat Zholayev on 16.01.17.
//  Copyright © 2017 Zholayev Toremurat. All rights reserved.
//

import UIKit
import SVProgressHUD
import Alamofire
import SwiftyJSON

class SearchViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UISearchControllerDelegate, UISearchResultsUpdating, UISearchBarDelegate, FilterResultDelegate  {
    var searchController : UISearchController!
    let label: UILabel = UILabel()
    let image: UIImageView = UIImageView()
    @IBOutlet weak var collectionView: UICollectionView!
    
    var results: [Ships] = []
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh(sender:)), for: UIControlEvents.valueChanged)
        
        return refreshControl
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        getLatestOrder(search: "")
        //Empty View
        
        label.font = UIFont.systemFont(ofSize: 17.0)
        label.textColor = UIColor.gray
        label.textAlignment = .center
        label.numberOfLines = 3
        label.isHidden = true
        image.isHidden = true
        label.text = "Список последних заказов пока пуст"
        image.image = UIImage(named: "dataSheet")
        view.addSubview(label)
        view.addSubview(image)
        image.snp.makeConstraints { (make) in
            make.width.equalTo(70)
            make.height.equalTo(70)
            make.centerX.equalToSuperview()
            make.top.equalToSuperview().offset(100)
            
            
        }
        label.snp.makeConstraints { (make) in
            make.top.equalTo(image.snp.bottom).offset(16)
            make.left.equalToSuperview().offset(16)
            make.right.equalToSuperview().offset(-16)
            
            
        }
        
        //CollectionView
        collectionView.delegate = self
        collectionView.dataSource = self
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 1
        collectionView!.collectionViewLayout = layout
        collectionView.register(UINib.init(nibName: "GoodsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "Good")
        collectionView.addSubview(refreshControl)
        //Search bar UI
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action: nil)
        self.searchController = UISearchController(searchResultsController:  nil)
        self.searchController.searchResultsUpdater = self
        self.searchController.delegate = self
        self.searchController.searchBar.delegate = self
        self.searchController.hidesNavigationBarDuringPresentation = false
        self.searchController.dimsBackgroundDuringPresentation = true
        self.navigationItem.titleView = searchController.searchBar
        self.definesPresentationContext = true
    }
    func showEmptyLabel(hidden:Bool)
    {
        
    }
    func refresh(sender:AnyObject) {
        refreshBegin { (x:Int) in
            //updateHere
            
            self.refreshControl.endRefreshing()
        }
    }
    func refreshBegin(refreshEnd:@escaping (Int) -> ()) {
        DispatchQueue.global().async {
            print("refreshing")
            
            self.getLatestOrder(search: "")
            sleep(2)
            
            DispatchQueue.main.async {
                refreshEnd(0)
                
            }
        }
    }
    
    @IBAction func filterButtonPressed(_ sender: UIBarButtonItem) {

    }
 
    func updateSearchResults(for searchController: UISearchController) {
    
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        getLatestOrder(search: searchBar.text!)
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        getLatestOrder(search: "")
    }
    
    func getFilterList(results: [Ships]) {
        self.results = results
        collectionView.reloadData()
        if results.count == 0 {
            label.isHidden = false
            image.isHidden = false
            label.text = "Нет результатов по запрошенным параметрам"
            //SVProgressHUD.showError(withStatus: "Нет результатов по запрошенным параметрам")
        }
        else {
            label.isHidden = true
            image.isHidden = true
        }
       
       
    }
    // MARK: ---------------------------COLLECTION VIEW
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Good", for: indexPath as IndexPath) as! GoodsCollectionViewCell
        cell.carNameLabel.text = "\(results[indexPath.row].from.titleRu) → \(results[indexPath.row].to.titleRu)"
        cell.carDescLabel.text = "\(results[indexPath.row].provisionalDate) · \(results[indexPath.row].distance) · \(results[indexPath.row].volume) м³ · \(results[indexPath.row].capacity) т "
        cell.carVolumeLabel.text = results[indexPath.row].companyName
        cell.priceLabel.text = "\(results[indexPath.row].price)"
        cell.carStatusLabel.text = self.getStatusName(id: results[indexPath.row].status)
        cell.carVolumeLabel.textColor = UIColor.black
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "SegueDetail", sender: results[indexPath.row])
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return results.count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 95)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SegueFilter" {
            if let vc = segue.destination as? FilterViewController {
                vc.delegate = self
            }
        }
        if segue.identifier == "SegueDetail" {
            if let vc = segue.destination as? DetailGoodsViewController {
                if let snd = sender as? Ships {
                    vc.detail = snd
                }
            }
            
        }

    }
    
    func getLatestOrder(search:String){
        SVProgressHUD.show()
        Alamofire.request(BaseUrl.mainUrl + "/api/v1/transporter/order/latest", method: .post,
                          parameters: ["device_token": BaseUrl.token,
                                       "search":search]).responseJSON { [weak self] response in
                            switch response.result {
                            case .success:
                                guard let vc = self, let data = response.data else {return }
                                let json = JSON(data:data)
                                print(json)
                                
                                if json["status"] == "success" {
                                    
                                    if json["result"].count == 0 {
                                        vc.label.isHidden = false
                                        vc.image.isHidden = false
                                        vc.label.text = "Список последних заказов пока пуст"
                                    }
                                    else {
                                        vc.label.isHidden = true
                                        vc.image.isHidden = true
                                    }
                                    
                                    vc.results.removeAll()
                                    for (_, subJson):(String, JSON) in json["result"] {
                                        if let new = Ships(json: subJson) {
                                            vc.results.append(new)
                                        }
                                    }
                                    SVProgressHUD.dismiss()
                                    vc.collectionView.reloadData()
                                    if json["result"].count == 0 && search != ""{
                                        
                                        SVProgressHUD.showError(withStatus: "По данному запросу нет результатов")
                                    }
                                }
                                else if json["status"] == "error" {
                                    SVProgressHUD.showError(withStatus: json["result"]["errors"].string)
                                }
                            case .failure(let error):
                                print(error)
                                SVProgressHUD.showError(withStatus: error.localizedDescription)
                                
                            }
        }
        
    }
}
