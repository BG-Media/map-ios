//
//  MainPageViewController.swift
//  MapPlus
//
//  Created by Toremurat Zholayev on 08.02.17.
//  Copyright © 2017 Zholayev Toremurat. All rights reserved.
//

import UIKit
import Alamofire
import UserNotifications
class MainPageViewController: UIViewController {
    var addButton:UIBarButtonItem!
    var pageMenu : CAPSPageMenu?
  
    override func viewDidLoad() {
        super.viewDidLoad()
        setupPageMenu()
        registerPushNotifications()

    }

    override func viewWillAppear(_ animated: Bool) {
        //self.navigationController?.navigationBar.isTranslucent = false
        super.viewWillAppear(animated)
     
       
    }
    override func viewWillDisappear(_ animated: Bool) {
       //self.navigationController?.navigationBar.isTranslucent = true
        super.viewWillDisappear(animated)
    }
    func buttonMethod() {
        print("Perform action")
    }
    
    func setupPageMenu(){
        // MARK: - UI Setup
        self.navigationController?.navigationBar.isOpaque = false
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.black]
       
        // MARK: - Scroll menu setup
        
        let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
        
        // Initialize view controllers to display and place in array
        var controllerArray : [UIViewController] = []
        
        let controller1 = storyBoard.instantiateViewController(withIdentifier: "GoodsVC") as! GoodsViewController
        controller1.title = "Мои грузы"
        controllerArray.append(controller1)
        let controller2 = storyBoard.instantiateViewController(withIdentifier: "GoodsVC") as! GoodsViewController
        
        controller2.title = "Мои транспорты"
        controller2.isTransport = true
        controllerArray.append(controller2)

        
        // Customize menu (Optional)
        let parameters: [CAPSPageMenuOption] = [
            .scrollMenuBackgroundColor(UIColor.white),
            .menuMargin(5),
            .viewBackgroundColor(UIColor.white),
            .selectionIndicatorColor(UIColor(red:0.98, green:0.37, blue:0.09, alpha:1.0)),
            .unselectedMenuItemLabelColor(UIColor.gray),
            .selectedMenuItemLabelColor(UIColor.black),
            .menuItemFont(UIFont.systemFont(ofSize: 16.0)),
            .menuHeight(33.0),
            .menuItemWidth(self.view.frame.width/2-10),
            .addBottomMenuHairline(true),
            .bottomMenuHairlineColor(UIColor(red:0.92, green:0.92, blue:0.95, alpha:1.0)),
            .enableHorizontalBounce(false),
            .selectionIndicatorHeight(2.0),
            
        ]
        
        // Initialize scroll menu
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0.0, y: 0.0, width: self.view.frame.width, height: self.view.frame.height), pageMenuOptions: parameters)
        
        self.addChildViewController(pageMenu!)
        self.view.addSubview(pageMenu!.view)
        
        pageMenu!.didMove(toParentViewController: self)
    }
    func registerPushNotifications() {
        DispatchQueue.main.async {
            let center = UNUserNotificationCenter.current()
            center.requestAuthorization(options: [.alert, .sound]) { (granted, error) in
                // actions based on whether notifications were authorized or not
            }
            UIApplication.shared.registerForRemoteNotifications()
        }
    }
    
    func didTapGoToLeft() {
        let currentIndex = pageMenu!.currentPageIndex
        
        if currentIndex > 0 {
            pageMenu!.moveToPage(currentIndex - 1)
        }
    }
    
    func didTapGoToRight() {
        let currentIndex = pageMenu!.currentPageIndex
        
        if currentIndex < pageMenu!.controllerArray.count {
            pageMenu!.moveToPage(currentIndex + 1)
        }
    }
    
    // MARK: - Container View Controller
    override var shouldAutomaticallyForwardAppearanceMethods : Bool {
        return true
    }
    
    override func shouldAutomaticallyForwardRotationMethods() -> Bool {
        return true
    }

}
extension AppDelegate {
    func registerDeviceToken(pushToken:String) {
            var isAgain = false
            
            if let tkn = UserDefaults.standard.value(forKey: "pushToken") as? String {
                if tkn == pushToken {
                    isAgain = true
                }
            }
            if isAgain == false {
                Alamofire.request("http://mapplus.bugingroup.kz/api/v1/transporter/account/update", method: .post, parameters:["device_os":"ios", "push_token":"\(pushToken)",
                    "device_token":BaseUrl.token])
                    .responseJSON { response in
                        if let json = response.result.value as? [String: AnyObject] {
                            print(json)
                            if let status = json["status"] as? String {
                                print(status)
                                if status == "success" {
                                    UserDefaults.standard.set(pushToken, forKey: "pushToken")
                                }
                            }
                        }
                        
                }
            }else {
                print("it is again")
        }
        
        
        
        }
    
}
