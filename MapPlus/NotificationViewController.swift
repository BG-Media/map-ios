//
//  NotificationViewController.swift
//  MapPlus
//
//  Created by Toremurat Zholayev on 26.01.17.
//  Copyright © 2017 Zholayev Toremurat. All rights reserved.
//

import UIKit
let reuseIdentifier = "Cell"
import SVProgressHUD
import Alamofire
import MapKit
import SwiftyJSON
class NotificationViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    
    
    var results: [Notifications] = []
    var locManager = CLLocationManager()
    var currentLocation: CLLocation!
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(NotificationCell.self, forCellReuseIdentifier: reuseIdentifier)
        // Do any additional setup after loading the view.
        
        locManager.requestWhenInUseAuthorization()
       
        getAllNotifications()
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return results.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if results[indexPath.row].disabled == 0 {
            showAlertController(sender: results[indexPath.row])
        }
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as! NotificationCell

        cell.titleLabel.text = getCategory(name: results[indexPath.row].type)
        if results[indexPath.row].disabled == 1 {
            cell.titleLabel.textColor = UIColor.gray
            cell.subTitleLabel.textColor = UIColor.gray
            cell.dateLabel.textColor = UIColor.gray
        }
        
        cell.subTitleLabel.text = results[indexPath.row].text
        let date = Date(timeIntervalSince1970: Double(results[indexPath.row].time))
        let formatter = DateFormatter()
        formatter.dateFormat = "hh:mm"
        let timeText = formatter.string(from: date)
        cell.dateLabel.text = timeText
        return cell
    }
    func getCategory(name:String)->String {
        switch name {
        case "findLocation":
            return "Запрос местоположения"
        default:
            return "Новое уведомление"
        }
    }
    
    func showAlertController(sender:Notifications){
        var lat = 0.0
        var long = 0.0
        if (CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways){
            currentLocation = locManager.location
            lat = currentLocation.coordinate.latitude
            long = currentLocation.coordinate.longitude
        }
        else {
            SVProgressHUD.showError(withStatus: "Включите в настройках местопложение для приложения MapPlus")
        }
        let alert = UIAlertController(title: "Выберите действие", message: nil, preferredStyle: .actionSheet)
        let action = UIAlertAction(title: "Отправить местоположение", style: .default) {  (act) in
            self.sendLocation(entity_id: sender.entity_id, long: lat, lat: long, notification_id: sender.id)
        }
        let cancel = UIAlertAction(title: "Закрыть", style: .cancel) { (act) in
            
        }
        alert.addAction(action)
        alert.addAction(cancel)
        if lat != 0.0 {
            present(alert, animated: true, completion: nil)
        }
       
        
    }
    
    func sendLocation(entity_id:Int, long:Double, lat:Double, notification_id:Int) {
        SVProgressHUD.show()
        print(entity_id, long, lat, notification_id)
        Alamofire.request(BaseUrl.mainUrl + BaseUrl.notifications, method: .post,
                          parameters: ["device_token": BaseUrl.token,
                            "entity_id": entity_id, "longitude": long,
                            "latitude": lat, "notification_id": notification_id]).responseJSON { [weak self] response in
                            switch response.result {
                            case .success:
                                guard let vc = self, let data = response.data else {return }
                                let json = JSON(data:data)
                                print(json)
                                
                                if json["status"] == "success" {
                                    SVProgressHUD.showSuccess(withStatus: "Успено отправлено")
                                    vc.tableView.reloadData()
                                }
                                    
                                else if json["status"] == "error" {
                                    SVProgressHUD.showError(withStatus: "Ошибка загрузки данных")
                                }
                            case .failure(let error):
                                print(error)
                                SVProgressHUD.showError(withStatus: error.localizedDescription)
                                
                            }
        }
    }
    func getAllNotifications()
    {
        SVProgressHUD.show()
        Alamofire.request(BaseUrl.mainUrl + BaseUrl.notifications, method: .post,
                          parameters: ["device_token": BaseUrl.token]).responseJSON { [weak self] response in
                            switch response.result {
                            case .success:
                                guard let vc = self, let data = response.data else {return }
                                let json = JSON(data:data)
                                print(json)
                                
                                if json["status"] == "success" {
                                    if json["result"].count == 0 {
                                        vc.showEmptyData(title: "У вас нет еще никаких уведомлений")
                                    }
                                    vc.results.removeAll()
                                    for (_, subJson):(String, JSON) in json["result"] {
                                        if let new = Notifications(json: subJson) {
                                            vc.results.append(new)
                                        }
                                    }
                                    SVProgressHUD.dismiss()
                                    vc.tableView.reloadData()
                                }
                                else if json["status"] == "error" {
                                    SVProgressHUD.showError(withStatus: "Ошибка загрузки данных")
                                }
                            case .failure(let error):
                                print(error)
                                SVProgressHUD.showError(withStatus: error.localizedDescription)
                                
                            }
        }
        
    }
    
    
    
    
}
class NotificationCell : UITableViewCell {
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    let titleLabel:UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 17.0)
        label.text = "Уведомление #1"
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
        
    }()
    let subTitleLabel : UILabel = {
       let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 13.0)
        label.textColor = UIColor.black
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 2
        return label
        
    }()
    let dateLabel : UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 13.0)
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    func setupViews(){
        backgroundColor = UIColor.white
        addSubview(titleLabel)
        addSubview(subTitleLabel)
        addSubview(dateLabel)
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-16-[v0]", options: NSLayoutFormatOptions(), metrics: nil, views: ["v0":titleLabel]))
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-8-[v0]", options: NSLayoutFormatOptions(), metrics: nil, views: ["v0":titleLabel]))
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-16-[v0]-16-|", options: NSLayoutFormatOptions(), metrics: nil, views: ["v0":subTitleLabel]))
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[v0]-3-[v1]-|", options: NSLayoutFormatOptions(), metrics: nil, views: ["v0":titleLabel, "v1":subTitleLabel]))
        
//        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[titleLabel]-[dateLabel]-16-|", options: NSLayoutFormatOptions(), metrics: nil, views: ["dateLabel":dateLabel, "titleLabel":titleLabel]))
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[titleLabel]-(<=1)-[dateLabel]-16-|", options: NSLayoutFormatOptions.alignAllCenterY, metrics: nil, views: ["dateLabel":dateLabel, "titleLabel":titleLabel]))
        
        
        
    }
}
