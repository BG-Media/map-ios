//
//  SecondPageTableViewController.swift
//  MapPlus
//
//  Created by Toremurat Zholayev on 03.02.17.
//  Copyright © 2017 Zholayev Toremurat. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import Alamofire
import SwiftyJSON
import SVProgressHUD
class SecondPageTableViewController: UITableViewController, UITextFieldDelegate {
    var marks:[Mark] = []
    var carTypes: [Model] = []
    var models:[Model] = []
    
    var selectedBrand = 0
    var selectedModel = 0
    var selectedType = 0
    var nextIndex = 2
    
    let markPicker = UIPickerView()
    let modelPicker = UIPickerView()
    let typePicker = UIPickerView()
    let insuraneDatePicker = UIDatePicker()
    let inspectionDatePicker = UIDatePicker()
    
    @IBOutlet weak var markTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var model: SkyFloatingLabelTextField!
    @IBOutlet weak var year: SkyFloatingLabelTextField!
    @IBOutlet weak var type: SkyFloatingLabelTextField!
    @IBOutlet weak var volume: SkyFloatingLabelTextField!
    @IBOutlet weak var capacity: SkyFloatingLabelTextField!
    @IBOutlet weak var gov_number: SkyFloatingLabelTextField!
    @IBOutlet weak var reg_place: SkyFloatingLabelTextField!
    @IBOutlet weak var date_tech_inspection: SkyFloatingLabelTextField!
    @IBOutlet weak var insurance_policy: SkyFloatingLabelTextField!
    
    @IBOutlet weak var desc: SkyFloatingLabelTextField!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        tableView.register(tblCell.self, forCellReuseIdentifier: "Cell")
        
        year.delegate = self
        setupPickerView(pickerView: markPicker, textField: markTextField, tagNum: 0)
        setupPickerView(pickerView: modelPicker, textField: model, tagNum: 1)
        setupPickerView(pickerView: typePicker, textField: type, tagNum: 2)
       
        setupDatePickerView(datePicker: insuraneDatePicker, textField: insurance_policy, selector: #selector(SecondPageTableViewController.insuranceTextFieldValueChanged))
        setupDatePickerView(datePicker: inspectionDatePicker, textField: date_tech_inspection, selector: #selector(SecondPageTableViewController.techInspectionValueChanged))
        
        //Call Options Api
        fetchListOptions()
    }
    func setupDatePickerView(datePicker: UIDatePicker, textField:UITextField, selector:Selector) {
        datePicker.datePickerMode = .date
        textField.inputView = datePicker
        datePicker.addTarget(self, action: selector, for: .valueChanged)
    }
    func setupPickerView(pickerView:UIPickerView, textField:UITextField, tagNum:Int) {
        pickerView.delegate = self
        pickerView.dataSource = self
        
        textField.inputView = pickerView
        pickerView.tag = tagNum
        
        pickerView.backgroundColor = .white
        pickerView.showsSelectionIndicator = true

        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red:0.23, green:0.64, blue:1.00, alpha:1.0)
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Выбрать", style: UIBarButtonItemStyle.plain, target: self, action: #selector(SecondPageTableViewController.donePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Закрыть", style: UIBarButtonItemStyle.plain, target: self, action: #selector(SecondPageTableViewController.cancelPicker))
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        textField.inputAccessoryView = toolBar

    }
    func insuranceTextFieldValueChanged(sender:UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.YYYY"
        insurance_policy.text = dateFormatter.string(from: sender.date)

    }
    func techInspectionValueChanged(sender:UIDatePicker) {
        
        let dateFormatter = DateFormatter()
       dateFormatter.dateFormat = "dd.MM.YYYY"
        date_tech_inspection.text = dateFormatter.string(from: sender.date)
    }
    
    func donePicker(){
        self.view.endEditing(true)
        
    }
    func cancelPicker(){
        self.view.endEditing(true)
    }
    
    // MARK: ---------------------------Mask for phone nubmer
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == self.year {
            let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            if (newString.characters.count == 5) {
                textField.resignFirstResponder()
                return false
            }
          
        }
        
        return true
    }
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 11
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 78
        }
        return 60
    }
    // MARK: ---------------------------Basic Functions
    @IBAction func nextButonPressed(_ sender: UIButton) {
        
        guard let brand = markTextField.text, !brand.isEmpty, let model = model.text, !model.isEmpty, let year = year.text, !year.isEmpty, let type = type.text, !type.isEmpty, let volume = volume.text, !volume.isEmpty, let capacity = capacity.text, !capacity.isEmpty,
        let gov_number = gov_number.text, !gov_number.isEmpty, let reg_place = reg_place.text, !reg_place.isEmpty, let date_tech_inspection = date_tech_inspection.text, !date_tech_inspection.isEmpty, let insurance_policy = insurance_policy.text, !insurance_policy.isEmpty, let desc = desc.text, !desc.isEmpty else {
            SVProgressHUD.showError(withStatus: "Пожалуйста, заполните все поля")
            
            return
        }


        let car = Car(device_token: BaseUrl.token, brand: "\(selectedBrand)", model: "\(selectedModel)", year: year, type: "\(selectedType)", volume: volume, capacity: capacity, gov_number: gov_number, reg_place: reg_place, date_tech_inspection: date_tech_inspection, insurance_policy: insurance_policy, desc: desc)
        car.addCar { (id) in
            print(id)
            self.moveToNextPageWith(index: self.nextIndex, title: "Фотографии транспорта")
            UserDefaults.standard.set(id, forKey: "car_id")
        }
       
    }
    
    // MARK: ---------------------------API REQUEST
    private func fetchListOptions() {
        SVProgressHUD.show(withStatus: "Загружаем данные...")
        Alamofire.request(BaseUrl.mainUrl + BaseUrl.listOptions, method: .post, parameters: ["device_token": BaseUrl.token]).responseJSON { [weak self] response in
            
            guard let vc = self, let data = response.data else {return }
            let json = JSON(data:data)
            print(json)
            
                
                    for (_, subJson):(String, JSON) in json["result"]["cars"] {
                        if let newMark = Mark(json: subJson) {
                            vc.marks.append(newMark)
                        }
                    }
                
               
                    for (_, subJson):(String, JSON) in json["result"]["car_types"] {
                        if let newType = Model(json: subJson) {
                            vc.carTypes.append(newType)
                        }
                    }
                
            
           
            SVProgressHUD.dismiss()
         
            
        }
    }
 

}
class tblCell : UITableViewCell {
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
extension UITextField{
    @IBInspectable var placeHolderColor: UIColor? {
        get {
            return self.placeHolderColor
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSForegroundColorAttributeName: newValue!])
        }
    }
}
extension SecondPageTableViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
      
        if pickerView.tag == 0 {
            return marks.count
        }
        else if pickerView.tag == 1{
            return models.count
        }
        else {
            return carTypes.count
        }
        
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView.tag == 0 {
            return marks[row].name
        }
        else if pickerView.tag == 1 {
            return models[row].name
        }
        else {
            return carTypes[row].name
        }
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView.tag == 0 {
            markTextField.text = marks[row].name
            model.text = ""
            models = marks[row].models
            selectedBrand = marks[row].id
        }
        else if pickerView.tag == 1 {
            model.text = models[row].name
            selectedModel = models[row].id
        }
        else {
            selectedType = carTypes[row].id
            type.text = carTypes[row].name
        }
    }
}
