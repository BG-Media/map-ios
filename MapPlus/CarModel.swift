//
//  Car.swift
//  MapPlus
//
//  Created by Toremurat Zholayev on 10.02.17.
//  Copyright © 2017 Zholayev Toremurat. All rights reserved.
//

import Foundation
import Alamofire
import SVProgressHUD

struct Car {
    
    private let device_token:String
    private let brand:String
    private let model:String
    private let year:String
    private let type:String
    private let volume:String
    private let capacity:String
    private let gov_number:String
    private let reg_place:String
    private let date_tech_inspection:String
    private let insurance_policy:String
    private let desc:String
    
    
    init(device_token:String, brand:String, model:String, year:String, type:String, volume:String, capacity:String, gov_number:String, reg_place:String, date_tech_inspection:String, insurance_policy:String, desc:String) {
        self.device_token = device_token
        self.brand = brand
        self.model = model
        self.year = year
        self.type = type
        self.volume = volume
        self.capacity = capacity
        self.gov_number = gov_number
        self.reg_place = reg_place
        self.date_tech_inspection = date_tech_inspection
        self.insurance_policy = insurance_policy
        self.desc = desc
        
    }
    
    func addCar(completion: @escaping (_ result:Int) -> Void)
    {
        SVProgressHUD.show()
        Alamofire.request(BaseUrl.mainUrl + BaseUrl.addCar, method: .post,
          parameters: ["device_token":device_token,
                       "brand":brand,
                       "model":model,
                       "year":year,
                       "type":type,
                       "volume":volume,
                       "capacity":capacity,
                       "gov_number":gov_number,
                       "reg_place":reg_place,
                       "date_tech_inspection":date_tech_inspection,
                       "insurance_policy":insurance_policy,
                       "description":desc]).responseJSON { response in
                        switch response.result {
                        case .success:
                            if let json = response.result.value as? [String:AnyObject] {
                                if let status = json["status"] as? String {
                                    print(json)
                                    if status == "success"{
                                        if let data = json["result"] as? [String:AnyObject] {
                                            if let id = data["id"] as? Int {
                                                print(id)
                                              
                                                completion(id)
                                            }
                                        }
                                        SVProgressHUD.dismiss()
                                    }
                                    else if status == "error" {
                                        if let data = json["result"] as? [String:AnyObject] {
                                            if let error = data["errors"] as? [String] {
                                                SVProgressHUD.showError(withStatus: error[0])
                                            }
                                        }
                                        
                                    }
                                }
                                
                            }
                        case .failure(let error):
                            print(error)
                            SVProgressHUD.showError(withStatus: error.localizedDescription)
                            
                        }
        }
    }
    
    func getMyCars(completion: @escaping () -> Void){
        SVProgressHUD.show()
        Alamofire.request(BaseUrl.mainUrl + BaseUrl.addCar, method: .post,
          parameters: ["device_token":device_token]).responseJSON { response in
                        switch response.result {
                        case .success:
                            if let json = response.result.value as? [String:AnyObject] {
                                if let status = json["status"] as? String {
                                    if status == "success"{
                                        if let data = json["result"] as? [String:AnyObject] {
                                           print(data)
                                        }
                                        SVProgressHUD.dismiss()
                                        completion()
                                        
                                    }
                                    else if status == "error" {
                                        if let data = json["result"] as? [String:AnyObject] {
                                            if let error = data["errors"] as? [String] {
                                                SVProgressHUD.showError(withStatus: error[0])
                                            }
                                        }
                                        
                                    }
                                }
                                
                            }
                        case .failure(let error):
                            print(error)
                            SVProgressHUD.showError(withStatus: error.localizedDescription)
                            
                        }
        }
    }
    
    
    

}
