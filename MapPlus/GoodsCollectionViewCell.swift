//
//  GoodsCollectionViewCell.swift
//  MapPlus
//
//  Created by Toremurat Zholayev on 10.01.17.
//  Copyright © 2017 Zholayev Toremurat. All rights reserved.
//

import UIKit

class GoodsCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var carStatusLabel: UILabel!
    @IBOutlet weak var carVolumeLabel: UILabel!
    @IBOutlet weak var carDescLabel: UILabel!
    @IBOutlet weak var carNameLabel: UILabel!
    

}
